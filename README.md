# LISEZ MOI #

Ceci est le projet réalisé à la session d’hiver 2014 par l’équipe Expresso.
L’équipe était composée de : Philippe Olivier, François Lemonnier-Lalonde,
Dominique Tremblay et François Moreau.

Le projet a été réalisé pour le cours de Génie logiciel orienté objet, donné par M. Jonathan Gaudreault à l'Université Laval.

Le projet consistait à développer un logiciel de modélisation de réseau de files d’attente, aussi appelé
un réseau de Jackson. Chaque nœud du réspeau représente une station de traitement et chaque arc orienté
représente un chemin possible d’une station à une autre. Le réseau possède toujours un point d’arrivée et de sortie.
Un tel réseau peut, par exemple, représenter la zone de sécurité d’un aéroport, l’urgence d’un hôpital ou une chaîne de montage.

Pour en savoir plus sur le projet, nous vous invitons à en visionner la présentation :

http://youtu.be/vbdkPlyn5E0

### Installation ###

* Installer le Java Runtime Environment (JRE), si ce n'est pas déjà fait.
* Télécharger et exécuter l'application : https://bitbucket.org/ulaval21610/glo2004/downloads/PerformNet-Expresso.jar
http://sourceforge.net/projects/performnet/

### Commentaires ###

Les commentaires sont les bienvenus! Vous pouvez nous contacter au expresso.performnet (à) gmail (point) com.

### Licence ###

Ce projet est publié sous la licence [BSD-3](http://opensource.org/licenses/BSD-3-Clause) :

Copyright (c) 2014, Philippe Olivier, François Lemonnier-Lalonde, Dominique Tremblay et François Moreau

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.