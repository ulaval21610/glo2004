/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet.modele;

import java.awt.Point;
import org.junit.Test;

public class MatriceTest {
    public MatriceTest() {}
    
    @Test
    public void testMatrice() {
        
        Reseau reseauTest = new Reseau();
        reseauTest.setLargeur(1000);
        reseauTest.setHauteur(1000);
        
//////// RESEAU WIKI ///////////////////////////////////////////////////////////
               
//        // stations
//        int id11 = Reseau.ID_ARRIVEE;
//        int id21 = reseauTest.addStation(new Point(2, 1));
//        int id31 = reseauTest.addStation(new Point(3, 1));
//        int id44 = reseauTest.addStation(new Point(4, 4));
//        int id54 = reseauTest.addStation(new Point(5, 4));
//        int id66 = reseauTest.addStation(new Point(6, 6));
//        int id76 = reseauTest.addStation(new Point(7, 6));
//        int id88 = Reseau.ID_SORTIE;
//        reseauTest.setStation(reseauTest.findStation(id11), null, null, "11");
//        reseauTest.setStation(reseauTest.findStation(id21), null, null, "21");
//        reseauTest.setStation(reseauTest.findStation(id31), null, null, "31");
//        reseauTest.setStation(reseauTest.findStation(id44), null, null, "44");
//        reseauTest.setStation(reseauTest.findStation(id54), null, null, "54");
//        reseauTest.setStation(reseauTest.findStation(id66), null, null, "66");
//        reseauTest.setStation(reseauTest.findStation(id76), null, null, "76");
//        reseauTest.setStation(reseauTest.findStation(id88), null, null, "88");
//        
//        // arcs
//        reseauTest.DEBUG_setDebit(reseauTest.addArc(reseauTest.findStation(id11), reseauTest.findStation(id21)), 100);
//        reseauTest.DEBUG_setDebit(reseauTest.addArc(reseauTest.findStation(id21), reseauTest.findStation(id31)), 100);
//        reseauTest.DEBUG_setDebit(reseauTest.addArc(reseauTest.findStation(id31), reseauTest.findStation(id11)), 100);
//        reseauTest.DEBUG_setDebit(reseauTest.addArc(reseauTest.findStation(id44), reseauTest.findStation(id21)), 33);
//        reseauTest.DEBUG_setDebit(reseauTest.addArc(reseauTest.findStation(id44), reseauTest.findStation(id31)), 33);
//        reseauTest.DEBUG_setDebit(reseauTest.addArc(reseauTest.findStation(id44), reseauTest.findStation(id54)), 33);
//        reseauTest.DEBUG_setDebit(reseauTest.addArc(reseauTest.findStation(id54), reseauTest.findStation(id44)), 50);
//        reseauTest.DEBUG_setDebit(reseauTest.addArc(reseauTest.findStation(id54), reseauTest.findStation(id66)), 50);
//        reseauTest.DEBUG_setDebit(reseauTest.addArc(reseauTest.findStation(id66), reseauTest.findStation(id31)), 50);
//        reseauTest.DEBUG_setDebit(reseauTest.addArc(reseauTest.findStation(id66), reseauTest.findStation(id76)), 50);
//        reseauTest.DEBUG_setDebit(reseauTest.addArc(reseauTest.findStation(id76), reseauTest.findStation(id66)), 100);
//        reseauTest.DEBUG_setDebit(reseauTest.addArc(reseauTest.findStation(id88), reseauTest.findStation(id54)), 33);
//        reseauTest.DEBUG_setDebit(reseauTest.addArc(reseauTest.findStation(id88), reseauTest.findStation(id76)), 33);
//        reseauTest.DEBUG_setDebit(reseauTest.addArc(reseauTest.findStation(id88), reseauTest.findStation(id88)), 33);
//        
//        // poids
//        reseauTest.setPoids(50);
        
//////// RESEAU STACKOVERFLOW //////////////////////////////////////////////////
        //https://stackoverflow.com/questions/21734598/how-to-balance-cycles-in-a-graph
        
        int Start = Reseau.ID_ARRIVEE;
        int NodeA = reseauTest.addStation(new Point(1, 1));
        int NodeB = reseauTest.addStation(new Point(2, 2));
        int End = Reseau.ID_SORTIE;
        
        reseauTest.setStation(reseauTest.findStation(Start), null, null, "Start", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(NodeA), null, null, "NodeA", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(NodeB), null, null, "NodeB", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(End), null, null, "End", null, null, null, null, null);
        
        reseauTest.DEBUG_setPA(reseauTest.addArc(reseauTest.findStation(Start), reseauTest.findStation(NodeA)), 200);
        reseauTest.DEBUG_setPA(reseauTest.addArc(reseauTest.findStation(NodeA), reseauTest.findStation(NodeB)), 10);
        reseauTest.DEBUG_setPA(reseauTest.addArc(reseauTest.findStation(NodeA), reseauTest.findStation(End)), 10);
        reseauTest.DEBUG_setPA(reseauTest.addArc(reseauTest.findStation(NodeB), reseauTest.findStation(NodeA)), 20);
        reseauTest.DEBUG_setPA(reseauTest.addArc(reseauTest.findStation(NodeB), reseauTest.findStation(End)), 10);
        
        //System.out.println(reseauTest.findStation(NodeA).getVitesseTraitement());
        
        reseauTest.setPoids(90);
        
        
//////// DISTRIBUTION ORIGINAL /////////////////////////////////////////////////
        
        float[] distributionTest = new float[reseauTest.getNbStations()];
        distributionTest = reseauTest.m_projet.buildDistribution();
        
        System.out.println("MATRICE DE DISTRIBUTION ORIGINALE:\n");
        
        reseauTest.DEBUG_printMatrice(distributionTest);
        
        float[][] transitionTest = new float[reseauTest.getNbStations()][reseauTest.getNbStations()];
        transitionTest = reseauTest.m_projet.buildTransition();

        System.out.println("\n\nMATRICE DE TRANSITION:\n");
        
        for (int i = 0; i < reseauTest.getNbStations(); ++i) {
            reseauTest.DEBUG_printMatrice(transitionTest[i]);
            System.out.println("");
        }
        
        System.out.println("\n\nMATRICE DE DISTRIBUTION APRES 10 ITÉRATIONS:\n");
        
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);

        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
        
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.convergeDistribution(distributionTest, transitionTest);
        
       reseauTest.DEBUG_printMatrice(distributionTest);
        
        reseauTest.m_projet.refreshPoidsStatsReseau();
        System.out.println(reseauTest.findStation(NodeA).getPoids());
        
        
        System.out.println(reseauTest.m_projet.getTempsMoyenEntiteReseau());
        
//        System.out.println(reseauTest.m_coherence.getIterationsMatrice());
    }
}
