/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet.modele;
import java.awt.Point;
import org.junit.Test;

/**
 *
 * @author win7pro64
 */
public class TestTempsMoyen {
    
        @Test
    public void testMatrice() {
        
        Reseau reseauTest = new Reseau();
        reseauTest.setLargeur(1000);
        reseauTest.setHauteur(1000);
        
        int Start = Reseau.ID_ARRIVEE;
        int NodeA = reseauTest.addStation(new Point(1, 1));
        int NodeB = reseauTest.addStation(new Point(2, 2));
        int NodeC = reseauTest.addStation(new Point(3, 3));
        int End = Reseau.ID_SORTIE;
        
        reseauTest.setStation(reseauTest.findStation(Start), null, null, "Start", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(NodeA), null, null, "NodeA", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(NodeB), null, null, "NodeB", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(NodeC), null, null, "NodeC", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(End), null, null, "End", null, null, null, null, null);
        
        reseauTest.DEBUG_setPA(reseauTest.addArc(reseauTest.findStation(Start), reseauTest.findStation(NodeA)), 50);
        reseauTest.DEBUG_setPA(reseauTest.addArc(reseauTest.findStation(Start), reseauTest.findStation(NodeC)), 50);
        
        //reseauTest.DEBUG_setPA(reseauTest.addArc(reseauTest.findStation(NodeA), reseauTest.findStation(NodeA)), 50);
        reseauTest.DEBUG_setPA(reseauTest.addArc(reseauTest.findStation(NodeA), reseauTest.findStation(NodeB)), 100);
        
        reseauTest.DEBUG_setPA(reseauTest.addArc(reseauTest.findStation(NodeB), reseauTest.findStation(End)), 100);
        
        reseauTest.DEBUG_setPA(reseauTest.addArc(reseauTest.findStation(NodeC), reseauTest.findStation(NodeB)), 50);
        reseauTest.DEBUG_setPA(reseauTest.addArc(reseauTest.findStation(NodeC), reseauTest.findStation(End)), 50);
//       
//        float[] distributionTest = new float[reseauTest.getNbStations()];
//        distributionTest = reseauTest.m_projet.buildDistributionTempsMoyen();
//        
//        System.out.println("MATRICE DE DISTRIBUTION ORIGINALE:\n");
//        
//        reseauTest.DEBUG_printMatrice(distributionTest);
//        
//        float[][] transitionTest = new float[reseauTest.getNbStations()][reseauTest.getNbStations()];
//        transitionTest = reseauTest.m_projet.buildTransition();
//
//        System.out.println("\n\nMATRICE DE TRANSITION:\n");
//        
//        for (int i = 0; i < reseauTest.getNbStations(); ++i) {
//            reseauTest.DEBUG_printMatrice(transitionTest[i]);
//            System.out.println("");
//        }
//        
//        
//        System.out.println("\n\nMATRICE DE DISTRIBUTION APRES 20 ITÉRATIONS:\n");
//        
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        distributionTest = reseauTest.m_projet.convergeDistribution(distributionTest, transitionTest);
//        
//        reseauTest.DEBUG_printMatrice(distributionTest);
        
        System.out.println(reseauTest.m_projet.getTempsMoyenEntiteReseau());
    }
    
}
