/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet.modele;

import java.awt.Point;
import org.junit.Test;



public class MasterReseau {
    Reseau reseauTest = new Reseau();
   
    @Test
    public void testReseau() {
    
//////// RÉSEAU ORIGINAL EN NOIR ///////////////////////////////////////////////
        
        reseauTest.setLargeur(1500);
        reseauTest.setHauteur(1500);
        
        int id_A = Reseau.ID_ARRIVEE;
        int id_1 = reseauTest.addStation(new Point(100, 200));
        int id_2 = reseauTest.addStation(new Point(100, 500));
        int id_3 = reseauTest.addStation(new Point(250, 650));
        int id_4 = reseauTest.addStation(new Point(200, 400));
        int id_5 = reseauTest.addStation(new Point(300, 200));
        int id_6 = reseauTest.addStation(new Point(400, 500));
        int id_7 = reseauTest.addStation(new Point(500, 700));
        int id_8 = reseauTest.addStation(new Point(600, 100));
        int id_9 = reseauTest.addStation(new Point(800, 200));
        int id_10 = reseauTest.addStation(new Point(800, 550));
        int id_11 = reseauTest.addStation(new Point(900, 400));
        int id_12 = reseauTest.addStation(new Point(1000, 50));
        int id_13 = reseauTest.addStation(new Point(1100, 200));
        int id_14 = reseauTest.addStation(new Point(1050, 700));
        int id_15 = reseauTest.addStation(new Point(1200, 500));
        int id_16 = reseauTest.addStation(new Point(1300, 300));
        int id_17 = reseauTest.addStation(new Point(700, 700));
        int id_18 = reseauTest.addStation(new Point(600, 500));
        int id_19 = reseauTest.addStation(new Point(800, 750));
        int id_20 = reseauTest.addStation(new Point(550, 800));
        int id_S = Reseau.ID_SORTIE;
 
        reseauTest.setStation(reseauTest.findStation(id_A), 50.0, 400.0, "A", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_1), null, null, "1", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_2), null, null, "2", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_3), null, null, "3", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_4), null, null, "4", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_5), null, null, "5", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_6), null, null, "6", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_7), null, null, "7", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_8), null, null, "8", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_9), null, null, "9", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_10), null, null, "10", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_11), null, null, "11", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_12), null, null, "12", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_13), null, null, "13", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_14), null, null, "14", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_15), null, null, "15", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_16), null, null, "16", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_17), null, null, "17", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_18), null, null, "18", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_19), null, null, "19", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_20), null, null, "20", null, null, null, null, null);
        reseauTest.setStation(reseauTest.findStation(id_S), 1400.0, 400.0, "S", null, null, null, null, null);
        
        reseauTest.addArc(reseauTest.findStation(id_A), reseauTest.findStation(id_1));
        reseauTest.addArc(reseauTest.findStation(id_A), reseauTest.findStation(id_2));
        reseauTest.addArc(reseauTest.findStation(id_1), reseauTest.findStation(id_5));
        reseauTest.addArc(reseauTest.findStation(id_2), reseauTest.findStation(id_4));
        reseauTest.addArc(reseauTest.findStation(id_3), reseauTest.findStation(id_2));
        reseauTest.addArc(reseauTest.findStation(id_4), reseauTest.findStation(id_5));
        reseauTest.addArc(reseauTest.findStation(id_4), reseauTest.findStation(id_6));
        reseauTest.addArc(reseauTest.findStation(id_6), reseauTest.findStation(id_18));
        reseauTest.addArc(reseauTest.findStation(id_7), reseauTest.findStation(id_17));
        reseauTest.addArc(reseauTest.findStation(id_8), reseauTest.findStation(id_9));
        reseauTest.addArc(reseauTest.findStation(id_9), reseauTest.findStation(id_8));
        reseauTest.addArc(reseauTest.findStation(id_10), reseauTest.findStation(id_11));
        reseauTest.addArc(reseauTest.findStation(id_10), reseauTest.findStation(id_19));
        reseauTest.addArc(reseauTest.findStation(id_11), reseauTest.findStation(id_15));
        reseauTest.addArc(reseauTest.findStation(id_11), reseauTest.findStation(id_16));
        reseauTest.addArc(reseauTest.findStation(id_12), reseauTest.findStation(id_11));
        reseauTest.addArc(reseauTest.findStation(id_12), reseauTest.findStation(id_13));
        reseauTest.addArc(reseauTest.findStation(id_13), reseauTest.findStation(id_12));
        reseauTest.addArc(reseauTest.findStation(id_15), reseauTest.findStation(id_16));
        reseauTest.addArc(reseauTest.findStation(id_16), reseauTest.findStation(id_S));
        reseauTest.addArc(reseauTest.findStation(id_17), reseauTest.findStation(id_20));
        reseauTest.addArc(reseauTest.findStation(id_18), reseauTest.findStation(id_8));
        reseauTest.addArc(reseauTest.findStation(id_18), reseauTest.findStation(id_10));
        reseauTest.addArc(reseauTest.findStation(id_19), reseauTest.findStation(id_18));
        reseauTest.addArc(reseauTest.findStation(id_20), reseauTest.findStation(id_7));

//        Coherence coherenceTest = new Coherence(reseauTest);
//        
//      
////////// RÉSEAU RÉPARÉ EN VERT /////////////////////////////////////////////////
//
//        // station-source
//        reseauTest.addArc(reseauTest.findStation(id_4), reseauTest.findStation(id_3));
//        coherenceTest = new Coherence(reseauTest);
//
//        
//        // station-puits
//        reseauTest.addArc(reseauTest.findStation(id_5), reseauTest.findStation(id_6));
//        coherenceTest = new Coherence(reseauTest);
//
//        
//        // cycle non-connexe
//        reseauTest.addArc(reseauTest.findStation(id_6), reseauTest.findStation(id_7));
//        reseauTest.addArc(reseauTest.findStation(id_17), reseauTest.findStation(id_18));
//        coherenceTest = new Coherence(reseauTest);
//
//        
//        // cycle-puits
//        reseauTest.addArc(reseauTest.findStation(id_9), reseauTest.findStation(id_18));
//        coherenceTest = new Coherence(reseauTest);
//
//        
//        // cycle-source
//        reseauTest.addArc(reseauTest.findStation(id_16), reseauTest.findStation(id_12));
//        coherenceTest = new Coherence(reseauTest);
//
//        
//        // station non-connexe
//        reseauTest.addArc(reseauTest.findStation(id_11), reseauTest.findStation(id_14));
//        reseauTest.addArc(reseauTest.findStation(id_14), reseauTest.findStation(id_10));
//        coherenceTest = new Coherence(reseauTest);
//
//        
////////// COHÉRENCE DU RÉSEAU ///////////////////////////////////////////////////
//        
//        coherenceTest = new Coherence(reseauTest);

        
//////// STATISTIQUES //////////////////////////////////////////////////////////
        
        
    }
    
    
    
}

