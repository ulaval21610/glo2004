/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet;

import PerformNet.modele.Reseau;
import java.awt.Point;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class StatsSimple {

    Reseau reseauTest = new Reseau();
   
    @Test
    public void testStats() {
        
        int arrivee = Reseau.ID_ARRIVEE;
        int station = reseauTest.addStation(new Point(500, 500));
        int sortie = Reseau.ID_SORTIE;
        
        reseauTest.addArc(reseauTest.findStation(arrivee), reseauTest.findStation(station));
        reseauTest.addArc(reseauTest.findStation(station), reseauTest.findStation(sortie));
        
        assertEquals(1, reseauTest.findStation(arrivee).getNbPostes());
        assertEquals(1, reseauTest.findStation(station).getNbPostes());
        assertEquals(1, reseauTest.findStation(sortie).getNbPostes());
        
        assertEquals(reseauTest.findStation(arrivee).getPoids(), reseauTest.findStation(station).getPoids(), 0);
        assertEquals(reseauTest.findStation(station).getPoids(), reseauTest.findStation(sortie).getPoids(), 0);
        
        assertEquals(100, reseauTest.findStation(arrivee).getVitesseTraitement());
        assertEquals(100, reseauTest.findStation(station).getVitesseTraitement());
        assertEquals(100, reseauTest.findStation(sortie).getVitesseTraitement());
        
        assertEquals(36, reseauTest.findStation(station).getTempsMoyenTraitement(), 0);
        
        assertEquals(0.9, reseauTest.findStation(station).getTauxUtilisation(), 0.0);
        
        assertEquals(8.1, reseauTest.findStation(station).getLongueurMoyenneFileAttente(), 0.01);
        
        assertEquals(324, reseauTest.findStation(station).getTempsMoyenEntiteTraverseFileAttente(), 0.1);
        
        assertEquals(360, reseauTest.findStation(station).getTempsMoyenEntiteTraverseStation(), 0.1);
        
        assertEquals(9, reseauTest.findStation(station).getNbMoyenEntitesDansStation(), 0.1);
        
        
    }
}
