/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet;
import PerformNet.modele.Reseau;
import PerformNet.modele.reseau.Noeud;
import java.awt.Point;
import java.awt.geom.Point2D;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author lampyridae
 */
public class ReseauTest {
    
    public ReseauTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        //Exécuté une fois avant l'ensemble des tests
    }
    
    @AfterClass
    public static void tearDownClass() {
        //Exécuté une fois après l'ensemble des tests
    }
    
    @Before
    public void setUp() {
        //Exécuté avant chaque test
    }
    
    @After
    public void tearDown() {
        //Exécuté après chaque test
    }

    /**
     * Vérifier le décompte des stations.
     */
    @Test
    public void testGetNbStations() {
        Reseau reseauTest = new Reseau();

        reseauTest.addStation(new Point(10, 10));
        reseauTest.addStation(new Point(10, 10));
        
        //2 stations + la station d'entrée + la station de sortie
        assertEquals(4, reseauTest.getNbStations());
    }
    

   /**
     * Obtenir une station en fonction d'une coordonnée.
     */
    @Test
    public void testFindStationXY() {
        Reseau reseauTest = new Reseau();

        int id_station = reseauTest.addStation(new Point(10, 10));

        Point pt_sur_station = new Point(
            10 + Noeud.LARGEUR_NOEUD_DEFAUT / 2,
            10 + Noeud.HAUTEUR_NOEUD_DEFAUT / 2);
        
        assertEquals(
                id_station,
                reseauTest.findStation(pt_sur_station).getID());
        
        Point pt_hors_station = new Point(
            10 + Noeud.LARGEUR_NOEUD_DEFAUT + 1,
            10 + Noeud.HAUTEUR_NOEUD_DEFAUT + 1);
        
        
        assertNull(reseauTest.findStation(pt_hors_station));
    }

    
   /**
     * Créer un arc.
     */
    @Test
    public void testArcs() {
        Reseau reseauTest = new Reseau();

        int id_station1 = reseauTest.addStation(new Point2D.Double(10, 10));
        int id_station2 = reseauTest.addStation(new Point2D.Double(20, 20));
        int id_station3 = reseauTest.addStation(new Point2D.Double(30, 30));
        
        reseauTest.addArc(
                reseauTest.findStation(id_station1),
                reseauTest.findStation(id_station2));
        
        assertEquals(
                100,
                reseauTest.getArc(id_station1, id_station2).getPoidsAbsolu(),
                0.001);

        assertNull(reseauTest.getArc(id_station1, id_station3));
        
    }
}
