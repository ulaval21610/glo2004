/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet.gui.panel;

import static PerformNet.gui.action.FilterSetter.initImageFilter;
import PerformNet.gui.control.ArcTableModel;
import PerformNet.gui.selection.SelectionElement;
import PerformNet.modele.Reseau;
import PerformNet.modele.ReseauModifieEvent;
import PerformNet.modele.ReseauModifieListener;
import PerformNet.modele.reseau.Arc;
import PerformNet.modele.reseau.Noeud;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

/**
 * classe qui permet d'afficher le jpanel des informations et données d'une
 * station sélectionnée
 *
 * @author Expresso
 */
public class PanneauProprietesStation extends javax.swing.JPanel {
    private static final long serialVersionUID = 4458868470623086771L;

    private transient SelectionElement m_selectionElement;
    private Noeud stationSelectionnee = null;
    private boolean isSyncing;
    private transient Reseau m_reseau;
    private DefaultTableModel defTableModel = new ArcTableModel();

    /**
     * créé une nouvelle instance de PanneauProprietesStation
     *
     * @param reseau
     * @param selectionElement
     */
    public PanneauProprietesStation(Reseau reseau, SelectionElement selectionElement) {
        initComponents();
        m_reseau = reseau;
        isSyncing = false;//permet de ne pas entrer en conflit pour les champs de données un temps entrées dans les champs, et un autre temps extraites des champs
        m_selectionElement = selectionElement;
        initListeners();
    }

    private void initListeners() {
        /*observateur d'un changement du poids absolu d'un arc dans la table d'arcs d'une station
         *
         */
        jTableArc.getModel().addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent e) {
                if (!isSyncing) {
                    if (stationSelectionnee != null) {
                        int selectedRowIndex = jTableArc.getSelectedRow();
                        //int selectedColumnIndex = jTableArc.getSelectedColumn();
                        Object cellContents = defTableModel.getValueAt(selectedRowIndex, 1);
                        m_selectionElement.select(stationSelectionnee.getArcsOut().get(selectedRowIndex), false);
                        try {
                            float poidsAbsolu = Float.valueOf(String.valueOf(cellContents));
                            m_reseau.setArc(stationSelectionnee.getArcsOut().get(selectedRowIndex), poidsAbsolu);
                        } catch (NumberFormatException exp) {

                        }
                        setjTableArc();
                    }
                }
            }
        });
        /*observateur de détection de changement d'informations du réseau et affichage des coordonnées de la station sélectionée lors d'un changement
         *
         */
        m_reseau.addReseauModifieListener(new ReseauModifieListener() {

            @Override
            public void reseauModifie(ReseauModifieEvent evt) {
                if (stationSelectionnee != null) {
                    syncControls();
                }
            }
        });
    }

    public void setStation(Noeud station, Arc arc) {
        stationSelectionnee = station;
        setTypeStation(
                !m_reseau.isStationDepart(station)
                && !m_reseau.isStationArrive(station));
        syncControls();
        setjTableArc();
        if (arc != null) {
            scrollRectToVisible(jScrollPaneArcs.getBounds());
            int arc_index = station.getArcOutIndexOf(arc);
            jTableArc.setRowSelectionInterval(arc_index, arc_index);
        } else {
            scrollRectToVisible(new Rectangle(0, 0, 1, 1));
        }
    }

    /**
     * mise à jour de la table d'arcs selon la station sélectionnée
     *
     */
    public void setjTableArc() {
        isSyncing = true;
        defTableModel.setRowCount(0);
        defTableModel.setColumnCount(0);

        int nombreDeRangee = stationSelectionnee.getArcsOut().size();

        defTableModel.addColumn("Arc");
        defTableModel.addColumn("Poids absolu");
        defTableModel.addColumn("Poids relatif");
        for (int i = 0; i < nombreDeRangee; i++) {
            Arc arc = stationSelectionnee.getArcsOut().get(i);
            defTableModel.insertRow(i, new Object[]{i});

            defTableModel.setValueAt(String.format("#%d", i + 1), i, 0);
            defTableModel.setValueAt(arc.getPoidsAbsolu(), i, 1);
            defTableModel.setValueAt(String.format("%.0f%%", 100 * arc.getPoidsRelatif()), i, 2);
        }

        isSyncing = false;
    }

    /**
     * mise à jour des champs relativement à la station sélectionnée
     *
     */
    public void syncControls() {
        isSyncing = true;
        Noeud Station = stationSelectionnee;

        //textfield
        jtfNomStation.setText(String.valueOf(stationSelectionnee.getNom()));
        jtfTauxEntree.setText(String.format("%.4g%n", stationSelectionnee.getTauxEntreeStation()));
        jtfTauxUtilisation.setText(String.format("%.4g%n", stationSelectionnee.getTauxUtilisation()));
        jtfPoids.setText(String.format("%.4g%n", stationSelectionnee.getPoids()));

        jtfNbMoyenEntites.setText(String.format("%.4g%n", stationSelectionnee.getNbMoyenEntitesDansStation()));
        jtfTempsMoyenAtt.setText(String.format("%.4g%n", stationSelectionnee.getTempsMoyenEntiteTraverseFileAttente()));
        jtfLongueurMoyenFile.setText(String.format("%.4g%n", stationSelectionnee.getLongueurMoyenneFileAttente()));
        jtfTempsTrait.setText(String.format("%.4g%n", stationSelectionnee.getTempsMoyenTraitement()));

        jspX.setValue(Station.getX());
        jspY.setValue(Station.getY());
        jspLargeur.setValue(Station.getRect().getWidth());
        jspHauteur.setValue(Station.getRect().getHeight());
        jspNbpostes.setValue(Station.getNbPostes());
        jspVitessetraitement.setValue(Station.getVitesseTraitement());
        jspTMT.setValue(Station.getTempsMoyenTraitement());
        jtfNomStation.setText(Station.getNom());

        RenderedImage img = stationSelectionnee.getFond();
        if (null != img) {
            Dimension dim = jlblFond.getPreferredSize();
            BufferedImage bi = resize(img, dim.width, dim.height);
            ImageIcon icon = new ImageIcon(bi);
            jlblFond.setIcon(icon);
        } else {
            jlblFond.setIcon(null);
        }
        jlblFond.revalidate();

        jbtEffacerFond.setEnabled(null != img);

        isSyncing = false;
    }

    public static BufferedImage resize(RenderedImage image, int maxwidth, int maxheight) {
        BufferedImage bi = new BufferedImage(maxwidth, maxheight, BufferedImage.TRANSLUCENT);
        Graphics2D g2d = bi.createGraphics();
        g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
        double scalex = 1;
        double scaley = 1;

        if (image.getWidth() > maxwidth) {
            scalex = (double) maxwidth / image.getWidth();
        }
        if (image.getHeight() > maxheight) {
            scaley = (double) maxheight / image.getHeight();
        }
        double scale = scalex < scaley
                ? scalex
                : scaley;
        AffineTransform at = AffineTransform.getScaleInstance(scale, scale);
        g2d.drawRenderedImage(image, at);

        g2d.dispose();
        return bi;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jlbNbPoste = new javax.swing.JLabel();
        jlbVitesseTrait = new javax.swing.JLabel();
        jlbTauxUtil = new javax.swing.JLabel();
        jlbPoids = new javax.swing.JLabel();
        jlbNbMoyenEntit = new javax.swing.JLabel();
        jlbTempsMoyenAtt = new javax.swing.JLabel();
        jspNbpostes = PerformNet.gui.control.JSpinnerFactory.JSpinnerWheelInt();
        jspVitessetraitement = PerformNet.gui.control.JSpinnerFactory.JSpinnerWheelInt();
        jlbTauxEntree = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jlbTempsTraitement = new javax.swing.JLabel();
        jlbY = new javax.swing.JLabel();
        jlbX = new javax.swing.JLabel();
        jspX = PerformNet.gui.control.JSpinnerFactory.JSpinnerWheelDouble(true);
        jlbNomStation = new javax.swing.JLabel();
        jtfNomStation = new javax.swing.JTextField();
        jtfTauxUtilisation = new javax.swing.JTextField();
        jtfPoids = new javax.swing.JTextField();
        jtfNbMoyenEntites = new javax.swing.JTextField();
        jtfTempsMoyenAtt = new javax.swing.JTextField();
        jtfTempsTrait = new javax.swing.JTextField();
        jtfTauxEntree = new javax.swing.JTextField();
        jtfLongueurMoyenFile = new javax.swing.JTextField();
        jlbLargeur = new javax.swing.JLabel();
        jlbHauteur = new javax.swing.JLabel();
        jspLargeur = PerformNet.gui.control.JSpinnerFactory.JSpinnerWheelDouble(false);
        jspHauteur = PerformNet.gui.control.JSpinnerFactory.JSpinnerWheelDouble(false);
        jspY = PerformNet.gui.control.JSpinnerFactory.JSpinnerWheelDouble(true);
        jPanel4 = new javax.swing.JPanel();
        jlblFond = new javax.swing.JLabel();
        jbtFond = new javax.swing.JButton();
        jbtEffacerFond = new javax.swing.JButton();
        jspTMT = PerformNet.gui.control.JSpinnerFactory.JSpinnerWheelDouble(false);
        jlbTMT = new javax.swing.JLabel();
        jScrollPaneArcs = new javax.swing.JScrollPane();
        jTableArc = new javax.swing.JTable();

        setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.Y_AXIS));

        jPanel2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 0, 0));

        java.awt.GridBagLayout jPanel1Layout = new java.awt.GridBagLayout();
        jPanel1Layout.columnWidths = new int[] {0, 6, 0, 6, 0, 6, 0};
        jPanel1Layout.rowHeights = new int[] {0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0};
        jPanel1.setLayout(jPanel1Layout);

        jlbNbPoste.setText("Nombre de postes");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 22, 0, 0);
        jPanel1.add(jlbNbPoste, gridBagConstraints);

        jlbVitesseTrait.setText("Vitesse de traitement");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 22, 0, 0);
        jPanel1.add(jlbVitesseTrait, gridBagConstraints);

        jlbTauxUtil.setText("Taux d'utilisation");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 30;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 22, 0, 0);
        jPanel1.add(jlbTauxUtil, gridBagConstraints);

        jlbPoids.setText("Poids");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 32;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 22, 0, 0);
        jPanel1.add(jlbPoids, gridBagConstraints);

        jlbNbMoyenEntit.setText("Nombre moyen d'entités");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 20;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 22, 0, 0);
        jPanel1.add(jlbNbMoyenEntit, gridBagConstraints);

        jlbTempsMoyenAtt.setText("Temps moyen d'attente");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 22;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 22, 0, 0);
        jPanel1.add(jlbTempsMoyenAtt, gridBagConstraints);

        jspNbpostes.setModel(new javax.swing.SpinnerNumberModel());
        jspNbpostes.setMaximumSize(new java.awt.Dimension(30, 22));
        jspNbpostes.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jspNbpostesStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 50;
        jPanel1.add(jspNbpostes, gridBagConstraints);

        jspVitessetraitement.setMaximumSize(new java.awt.Dimension(30, 22));
        jspVitessetraitement.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jspVitessetraitementStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 50;
        jPanel1.add(jspVitessetraitement, gridBagConstraints);

        jlbTauxEntree.setText("Taux d'entrée");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 24;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 22, 0, 0);
        jPanel1.add(jlbTauxEntree, gridBagConstraints);

        jLabel12.setText("Longueur moyenne file d'attente");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 26;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 22, 0, 0);
        jPanel1.add(jLabel12, gridBagConstraints);

        jlbTempsTraitement.setText("Temps de traitement");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 28;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 22, 0, 0);
        jPanel1.add(jlbTempsTraitement, gridBagConstraints);

        jlbY.setText("Position Y");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 22, 0, 0);
        jPanel1.add(jlbY, gridBagConstraints);

        jlbX.setText("Position X");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 22, 0, 0);
        jPanel1.add(jlbX, gridBagConstraints);

        jspX.setMaximumSize(new java.awt.Dimension(30, 22));
        jspX.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jspXStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 50;
        jPanel1.add(jspX, gridBagConstraints);

        jlbNomStation.setText("Nom de station");
        jlbNomStation.setToolTipText("");
        jlbNomStation.setMaximumSize(new java.awt.Dimension(189, 16));
        jlbNomStation.setMinimumSize(new java.awt.Dimension(189, 16));
        jlbNomStation.setPreferredSize(new java.awt.Dimension(189, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.ipady = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(12, 22, 0, 0);
        jPanel1.add(jlbNomStation, gridBagConstraints);

        jtfNomStation.setDragEnabled(true);
        jtfNomStation.setMaximumSize(new java.awt.Dimension(30, 25));
        jtfNomStation.setMinimumSize(new java.awt.Dimension(30, 25));
        jtfNomStation.setPreferredSize(new java.awt.Dimension(30, 25));
        jtfNomStation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfNomStationActionPerformed(evt);
            }
        });
        jtfNomStation.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtfNomStationFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.insets = new java.awt.Insets(12, 0, 0, 0);
        jPanel1.add(jtfNomStation, gridBagConstraints);

        jtfTauxUtilisation.setEditable(false);
        jtfTauxUtilisation.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jtfTauxUtilisation.setText("jTextField1");
        jtfTauxUtilisation.setMinimumSize(new java.awt.Dimension(69, 25));
        jtfTauxUtilisation.setPreferredSize(new java.awt.Dimension(69, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 30;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 50;
        jPanel1.add(jtfTauxUtilisation, gridBagConstraints);

        jtfPoids.setEditable(false);
        jtfPoids.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jtfPoids.setText("jTextField1");
        jtfPoids.setMinimumSize(new java.awt.Dimension(69, 25));
        jtfPoids.setPreferredSize(new java.awt.Dimension(69, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 32;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 20, 0);
        jPanel1.add(jtfPoids, gridBagConstraints);

        jtfNbMoyenEntites.setEditable(false);
        jtfNbMoyenEntites.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jtfNbMoyenEntites.setText("jTextField1");
        jtfNbMoyenEntites.setMinimumSize(new java.awt.Dimension(69, 25));
        jtfNbMoyenEntites.setPreferredSize(new java.awt.Dimension(69, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 20;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 50;
        jPanel1.add(jtfNbMoyenEntites, gridBagConstraints);

        jtfTempsMoyenAtt.setEditable(false);
        jtfTempsMoyenAtt.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jtfTempsMoyenAtt.setText("jTextField1");
        jtfTempsMoyenAtt.setMinimumSize(new java.awt.Dimension(69, 25));
        jtfTempsMoyenAtt.setPreferredSize(new java.awt.Dimension(69, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 22;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 50;
        jPanel1.add(jtfTempsMoyenAtt, gridBagConstraints);

        jtfTempsTrait.setEditable(false);
        jtfTempsTrait.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jtfTempsTrait.setText("jTextField1");
        jtfTempsTrait.setMinimumSize(new java.awt.Dimension(69, 25));
        jtfTempsTrait.setPreferredSize(new java.awt.Dimension(69, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 28;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 50;
        jPanel1.add(jtfTempsTrait, gridBagConstraints);

        jtfTauxEntree.setEditable(false);
        jtfTauxEntree.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jtfTauxEntree.setText("jTextField1");
        jtfTauxEntree.setMinimumSize(new java.awt.Dimension(69, 25));
        jtfTauxEntree.setPreferredSize(new java.awt.Dimension(69, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 24;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 50;
        jPanel1.add(jtfTauxEntree, gridBagConstraints);

        jtfLongueurMoyenFile.setEditable(false);
        jtfLongueurMoyenFile.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jtfLongueurMoyenFile.setText("jTextField1");
        jtfLongueurMoyenFile.setMinimumSize(new java.awt.Dimension(69, 25));
        jtfLongueurMoyenFile.setPreferredSize(new java.awt.Dimension(69, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 26;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 50;
        jPanel1.add(jtfLongueurMoyenFile, gridBagConstraints);

        jlbLargeur.setText("Largeur");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 22, 0, 0);
        jPanel1.add(jlbLargeur, gridBagConstraints);

        jlbHauteur.setText("Hauteur");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 22, 0, 0);
        jPanel1.add(jlbHauteur, gridBagConstraints);

        jspLargeur.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jspLargeurStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 50;
        jPanel1.add(jspLargeur, gridBagConstraints);

        jspHauteur.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jspHauteurStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 50;
        jPanel1.add(jspHauteur, gridBagConstraints);

        jspY.setMaximumSize(new java.awt.Dimension(30, 22));
        jspY.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jspYStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 50;
        jPanel1.add(jspY, gridBagConstraints);

        jlblFond.setBackground(new java.awt.Color(255, 0, 0));
        jlblFond.setMaximumSize(new java.awt.Dimension(70, 70));
        jlblFond.setMinimumSize(new java.awt.Dimension(70, 70));
        jlblFond.setPreferredSize(new java.awt.Dimension(70, 70));
        jPanel4.add(jlblFond);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(10, 22, 0, 0);
        jPanel1.add(jPanel4, gridBagConstraints);

        jbtFond.setText("Image de fond…");
        jbtFond.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtFondActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        jPanel1.add(jbtFond, gridBagConstraints);

        jbtEffacerFond.setText("Effacer");
        jbtEffacerFond.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtEffacerFondActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel1.add(jbtEffacerFond, gridBagConstraints);

        jspTMT.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jspTMTStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 18;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jspTMT, gridBagConstraints);

        jlbTMT.setText("Temps moyen de traitement");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 18;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 22, 0, 0);
        jPanel1.add(jlbTMT, gridBagConstraints);

        jPanel2.add(jPanel1);

        add(jPanel2);

        jScrollPaneArcs.setPreferredSize(new java.awt.Dimension(0, 402));

        jTableArc.setModel(defTableModel);
        jTableArc.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableArcMouseClicked(evt);
            }
        });
        jScrollPaneArcs.setViewportView(jTableArc);
        if (jTableArc.getColumnModel().getColumnCount() > 0) {
            jTableArc.getColumnModel().getColumn(0).setResizable(false);
            jTableArc.getColumnModel().getColumn(2).setResizable(false);
        }

        add(jScrollPaneArcs);
    }// </editor-fold>//GEN-END:initComponents
    /*détection d'un changement de valeur du jspinner de la coordonnée x de la station sélectionnée
     *
     */
    private void jspXStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jspXStateChanged
        if (isSyncing == false) {

            if (null != stationSelectionnee) {
                double coordX = ((Double) jspX.getValue());
                m_reseau.setStation(stationSelectionnee, coordX, null, null, null, null, null, null, null);
                m_reseau.enregistrerChangement();
            }
        }
    }//GEN-LAST:event_jspXStateChanged

    /*détection d'un changement de valeur du jspinner du nombre de postes de la station sélectionnée
     *
     */
    private void jspNbpostesStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jspNbpostesStateChanged
        if (isSyncing == false) {

            if (null != stationSelectionnee) {
                int nbDeposte = ((Integer) jspNbpostes.getValue());
                m_reseau.setStation(stationSelectionnee, null, null, null, nbDeposte, null, null, null, null);
                syncControls();
                m_reseau.enregistrerChangement();
            }
        }
    }//GEN-LAST:event_jspNbpostesStateChanged
    /*détection d'un changement de valeur du jspinner de la vitesse de traitement des postes de la station sélectionnée
     *
     */
    private void jspVitessetraitementStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jspVitessetraitementStateChanged
        if (isSyncing == false) {

            if (null != stationSelectionnee) {
                int vitesseTraitement = ((Integer) jspVitessetraitement.getValue());
                m_reseau.setStation(stationSelectionnee, null, null, null, null, vitesseTraitement, null, null, null);
                syncControls();
                m_reseau.enregistrerChangement();
            }
        }
    }//GEN-LAST:event_jspVitessetraitementStateChanged


    private void jTableArcMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableArcMouseClicked
    }//GEN-LAST:event_jTableArcMouseClicked
    /*détection de l'action activée lors d'un changement de nom de la station dans le champ de son nom
     *
     */
    private void jtfNomStationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfNomStationActionPerformed
        String nouveauNomStation = jtfNomStation.getText();
        m_reseau.setStation(stationSelectionnee, null, null, nouveauNomStation, null, null, null, null, null);
    }//GEN-LAST:event_jtfNomStationActionPerformed
    /*détection de l'action activée lors d'un changement de nom de la station dans le champ de son nom
     *
     */
    private void jtfNomStationFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtfNomStationFocusLost
        String nouveauNomStation = jtfNomStation.getText();
        m_reseau.setStation(stationSelectionnee, null, null, nouveauNomStation, null, null, null, null, null);
    }//GEN-LAST:event_jtfNomStationFocusLost

    private void jspLargeurStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jspLargeurStateChanged
        if (isSyncing == false) {

            if (null != stationSelectionnee) {
                Double largeur = ((Double) jspLargeur.getValue());
                m_reseau.setStation(stationSelectionnee, null, null, null, null, null, largeur, null, null);
                m_reseau.enregistrerChangement();

            }
        }
    }//GEN-LAST:event_jspLargeurStateChanged

    private void jspHauteurStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jspHauteurStateChanged
        if (isSyncing == false) {

            if (null != stationSelectionnee) {
                double hauteur = ((double) jspHauteur.getValue());
                m_reseau.setStation(stationSelectionnee, null, null, null, null, null, null, hauteur, null);
                m_reseau.enregistrerChangement();

            }
        }
    }//GEN-LAST:event_jspHauteurStateChanged

    private void jspYStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jspYStateChanged
        if (isSyncing == false) {

            if (null != stationSelectionnee) {
                double coordY = ((Double) jspY.getValue());
                m_reseau.setStation(stationSelectionnee, null, coordY, null, null, null, null, null, null);
                m_reseau.enregistrerChangement();

            }
        }
    }//GEN-LAST:event_jspYStateChanged

    private void jbtFondActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtFondActionPerformed
        JFileChooser chooser = new JFileChooser();

        initImageFilter(chooser, true, true, true, true, false);
        chooser.removeChoosableFileFilter(null);

        chooser.setDialogTitle("Insérer une image");

        int returnVal = chooser.showOpenDialog(this);
        if (returnVal != JFileChooser.APPROVE_OPTION) {
            return;
        }

        try {
            String filename = chooser.getSelectedFile().getCanonicalPath();
            m_reseau.setFondStation(stationSelectionnee, filename);
        } catch (IOException ex) {
            Logger.getLogger(PanneauProprietesStation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jbtFondActionPerformed

    private void jbtEffacerFondActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtEffacerFondActionPerformed
        m_reseau.setFondStation(stationSelectionnee, null);
    }//GEN-LAST:event_jbtEffacerFondActionPerformed

    private void jspTMTStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jspTMTStateChanged
        if (isSyncing == false) {

            if (null != stationSelectionnee) {
                double TMT = ((Double) jspTMT.getValue());
                m_reseau.setStation(stationSelectionnee, null, null, null, null, null, null, null, TMT);
                syncControls();
                m_reseau.enregistrerChangement();

            }
        }
    }//GEN-LAST:event_jspTMTStateChanged

    public void setTypeStation(boolean visible) {
        jlbNbMoyenEntit.setVisible(visible);
        jlbNbPoste.setVisible(visible);
        jlbTauxUtil.setVisible(visible);
        jlbTempsTraitement.setVisible(visible);
        jlbVitesseTrait.setVisible(visible);
        jlbTempsMoyenAtt.setVisible(visible);
        jLabel12.setVisible(visible);
        jlbPoids.setVisible(visible);
        jlbTMT.setVisible(visible);

        jspNbpostes.setVisible(visible);
        jspVitessetraitement.setVisible(visible);
        jspTMT.setVisible(visible);
        jtfTempsMoyenAtt.setVisible(visible);
        jtfTauxUtilisation.setVisible(visible);
        jtfTempsTrait.setVisible(visible);
        jtfLongueurMoyenFile.setVisible(visible);
        jtfPoids.setVisible(visible);
        jtfNbMoyenEntites.setVisible(visible);

        revalidate();
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel12;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPaneArcs;
    private javax.swing.JTable jTableArc;
    private javax.swing.JButton jbtEffacerFond;
    private javax.swing.JButton jbtFond;
    private javax.swing.JLabel jlbHauteur;
    private javax.swing.JLabel jlbLargeur;
    private javax.swing.JLabel jlbNbMoyenEntit;
    private javax.swing.JLabel jlbNbPoste;
    private javax.swing.JLabel jlbNomStation;
    private javax.swing.JLabel jlbPoids;
    private javax.swing.JLabel jlbTMT;
    private javax.swing.JLabel jlbTauxEntree;
    private javax.swing.JLabel jlbTauxUtil;
    private javax.swing.JLabel jlbTempsMoyenAtt;
    private javax.swing.JLabel jlbTempsTraitement;
    private javax.swing.JLabel jlbVitesseTrait;
    private javax.swing.JLabel jlbX;
    private javax.swing.JLabel jlbY;
    private javax.swing.JLabel jlblFond;
    private javax.swing.JSpinner jspHauteur;
    private javax.swing.JSpinner jspLargeur;
    private javax.swing.JSpinner jspNbpostes;
    private javax.swing.JSpinner jspTMT;
    private javax.swing.JSpinner jspVitessetraitement;
    private javax.swing.JSpinner jspX;
    private javax.swing.JSpinner jspY;
    private javax.swing.JTextField jtfLongueurMoyenFile;
    private javax.swing.JTextField jtfNbMoyenEntites;
    private javax.swing.JTextField jtfNomStation;
    private javax.swing.JTextField jtfPoids;
    private javax.swing.JTextField jtfTauxEntree;
    private javax.swing.JTextField jtfTauxUtilisation;
    private javax.swing.JTextField jtfTempsMoyenAtt;
    private javax.swing.JTextField jtfTempsTrait;
    // End of variables declaration//GEN-END:variables
}
