/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet.gui.panel;

import PerformNet.gui.MainJFrame;
import static PerformNet.gui.action.FilterSetter.initImageFilter;
import PerformNet.gui.selection.SelectionElement;
import PerformNet.gui.selection.SelectionGraphiqueEvent;
import PerformNet.gui.selection.SelectionGraphiqueListener;
import PerformNet.modele.Reseau;
import PerformNet.modele.ReseauModifieEvent;
import PerformNet.modele.ReseauModifieListener;
import PerformNet.modele.reseau.Graphique;
import java.io.IOException;
import static java.nio.file.Files.isReadable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

/**
 *
 * @author Expresso
 */
public class PanneauProprietesImage extends javax.swing.JPanel {
    private static final long serialVersionUID = -5225334675176157331L;

    private transient SelectionElement m_selectionElement;

    private Graphique graphiqueSelectionnee = null;
    private boolean isSyncing;
    private transient Reseau m_reseau;

    /**
     * Creates new form PanneauProprietesImage
     */
    public PanneauProprietesImage() {
        initComponents();
    }

    public PanneauProprietesImage(Reseau reseau, SelectionElement selectionElement) {
        initComponents();
        m_reseau = reseau;
        isSyncing = false;//permet de ne pas entrer en conflit pour les champs de données un temps entrées dans les champs, et un autre temps extraites des champs
        m_selectionElement = selectionElement;
        initListeners();
    }

    private void initListeners() {
        /*observateur de sélection d'une image dans le plan et affichage de ses informations
         *
         */
        m_selectionElement.addSelectionGraphiqueListener(new SelectionGraphiqueListener() {

            @Override
            public void graphiqueSelectionne(SelectionGraphiqueEvent event) {
                graphiqueSelectionnee = event.getGraphique();
                setJSpinners(event.getGraphique());
            }
        });

        /*observateur de détection de changement d'informations du réseau et affichage des coordonnées de l'image sélectionée lors d'un changement
         *
         */
        m_reseau.addReseauModifieListener(new ReseauModifieListener() {

            @Override
            public void reseauModifie(ReseauModifieEvent evt) {
                isSyncing = true;
                if (graphiqueSelectionnee != null) {
                    setJSpinners(graphiqueSelectionnee);
                }
                isSyncing = false;
            }
        });
    }

    /**
     * mise à jour des champs des jspinners des coordonnées x et y de l'image
     * mise en paramètres
     *
     * @param graphique
     */
    public void setJSpinners(Graphique graphique) {
        isSyncing = true;

        jspX.setValue(graphique.getPosition().getX());
        jspY.setValue(graphique.getPosition().getY());
        jspLargeur.setValue(graphique.getDimension().getX());
        jspHauteur.setValue(graphique.getDimension().getY());

        jtfPath.setText(graphique.getPath());
        jtfNom.setText(graphique.getNom());

        isSyncing = false;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jCheckBox1 = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jToggleButton1 = new javax.swing.JToggleButton();
        jlbNom = new javax.swing.JLabel();
        jlbPath = new javax.swing.JLabel();
        jtfNom = new javax.swing.JTextField();
        btnPath = new javax.swing.JButton();
        jtfPath = new javax.swing.JTextField();
        jlbHauteur = new javax.swing.JLabel();
        jlbLargeur = new javax.swing.JLabel();
        jlbY = new javax.swing.JLabel();
        jlbX = new javax.swing.JLabel();
        jspX = PerformNet.gui.control.JSpinnerFactory.JSpinnerWheelDouble(true);
        jspY = PerformNet.gui.control.JSpinnerFactory.JSpinnerWheelDouble(true);
        jspHauteur = PerformNet.gui.control.JSpinnerFactory.JSpinnerWheelDouble(false);
        jspLargeur = PerformNet.gui.control.JSpinnerFactory.JSpinnerWheelDouble(false);
        jchkDeplacable = new javax.swing.JCheckBox();

        jCheckBox1.setText("Visible");

        setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.Y_AXIS));

        jPanel2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 0, 0));

        java.awt.GridBagLayout jPanel1Layout = new java.awt.GridBagLayout();
        jPanel1Layout.columnWidths = new int[] {0, 6, 0, 6, 0, 6, 0, 6, 0};
        jPanel1Layout.rowHeights = new int[] {0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0};
        jPanel1.setLayout(jPanel1Layout);

        jToggleButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/icones_menu/link.png"))); // NOI18N
        jToggleButton1.setToolTipText("");
        jToggleButton1.setMaximumSize(new java.awt.Dimension(25, 25));
        jToggleButton1.setMinimumSize(new java.awt.Dimension(25, 25));
        jToggleButton1.setPreferredSize(new java.awt.Dimension(25, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel1.add(jToggleButton1, gridBagConstraints);

        jlbNom.setText("Nom:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(15, 20, 0, 0);
        jPanel1.add(jlbNom, gridBagConstraints);

        jlbPath.setText("Chemin d'accès:");
        jlbPath.setMaximumSize(new java.awt.Dimension(94, 22));
        jlbPath.setMinimumSize(new java.awt.Dimension(94, 22));
        jlbPath.setPreferredSize(new java.awt.Dimension(94, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 0);
        jPanel1.add(jlbPath, gridBagConstraints);

        jtfNom.setText("jTextField1");
        jtfNom.setDragEnabled(true);
        jtfNom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfNomActionPerformed(evt);
            }
        });
        jtfNom.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtfNomFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(15, 5, 0, 0);
        jPanel1.add(jtfNom, gridBagConstraints);

        btnPath.setText("...");
        btnPath.setMaximumSize(new java.awt.Dimension(25, 25));
        btnPath.setMinimumSize(new java.awt.Dimension(25, 25));
        btnPath.setPreferredSize(new java.awt.Dimension(25, 25));
        btnPath.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPathActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel1.add(btnPath, gridBagConstraints);

        jtfPath.setEditable(false);
        jtfPath.setText("jTextField2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 45, 0, 0);
        jPanel1.add(jtfPath, gridBagConstraints);

        jlbHauteur.setText("Hauteur");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 0);
        jPanel1.add(jlbHauteur, gridBagConstraints);

        jlbLargeur.setText("Largeur");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 0);
        jPanel1.add(jlbLargeur, gridBagConstraints);

        jlbY.setText("Position Y");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 0);
        jPanel1.add(jlbY, gridBagConstraints);

        jlbX.setText("Position X");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 0);
        jPanel1.add(jlbX, gridBagConstraints);

        jspX.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jspXStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.ipadx = 62;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel1.add(jspX, gridBagConstraints);

        jspY.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jspYStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.ipadx = 62;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel1.add(jspY, gridBagConstraints);

        jspHauteur.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jspHauteurStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.ipadx = 62;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel1.add(jspHauteur, gridBagConstraints);

        jspLargeur.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jspLargeurStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.ipadx = 62;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel1.add(jspLargeur, gridBagConstraints);

        jchkDeplacable.setSelected(true);
        jchkDeplacable.setText("Déplaçable");
        jchkDeplacable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jchkDeplacableActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 16;
        jPanel1.add(jchkDeplacable, gridBagConstraints);

        jPanel2.add(jPanel1);

        add(jPanel2);
    }// </editor-fold>//GEN-END:initComponents

    private void jtfNomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfNomActionPerformed
        String nouveauNomImage = jtfNom.getText();
        m_reseau.setGraphique(graphiqueSelectionnee, null, null, nouveauNomImage,
                null, null, null, null, jToggleButton1.isSelected());
       
    }//GEN-LAST:event_jtfNomActionPerformed

    private void jtfNomFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtfNomFocusLost
        String nouveauNomImage = jtfNom.getText();
        m_reseau.setGraphique(graphiqueSelectionnee, null, null, nouveauNomImage,
                null, null, null, null, jToggleButton1.isSelected());
    }//GEN-LAST:event_jtfNomFocusLost

    private void jspXStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jspXStateChanged
        if (isSyncing == false) {
            if (null != graphiqueSelectionnee) {
                double posX = ((Double) jspX.getValue());

                m_reseau.setGraphique(graphiqueSelectionnee, posX, null, null, null,
                        null, null, null, jToggleButton1.isSelected());
            }
        }
    }//GEN-LAST:event_jspXStateChanged

    private void jspYStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jspYStateChanged
        if (isSyncing == false) {
            if (null != graphiqueSelectionnee) {
                double posY = ((Double) jspY.getValue());
                m_reseau.setGraphique(graphiqueSelectionnee, null, posY, null, null,
                        null, null, null, jToggleButton1.isSelected());
            }
        }
    }//GEN-LAST:event_jspYStateChanged

    private void jspHauteurStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jspHauteurStateChanged
        if (isSyncing == false) {
            if (null != graphiqueSelectionnee) {
                double hauteur = ((Double) jspHauteur.getValue());
                m_reseau.setGraphique(graphiqueSelectionnee, null, null, null, null,
                        hauteur, null, null, jToggleButton1.isSelected());
            }
        }
    }//GEN-LAST:event_jspHauteurStateChanged

    private void jspLargeurStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jspLargeurStateChanged
        if (isSyncing == false) {
            if (null != graphiqueSelectionnee) {
                double largeur = ((Double) jspLargeur.getValue());
                m_reseau.setGraphique(graphiqueSelectionnee, null, null, null, largeur,
                        null, null, null, jToggleButton1.isSelected());
            }
        }
    }//GEN-LAST:event_jspLargeurStateChanged

    private void jchkDeplacableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jchkDeplacableActionPerformed
        m_reseau.setGraphique(graphiqueSelectionnee, null, null, null, null, null, null,
                jchkDeplacable.isSelected(), jToggleButton1.isSelected());
    }//GEN-LAST:event_jchkDeplacableActionPerformed

    private void btnPathActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPathActionPerformed
        String m_fileName;

        m_fileName = "";

        JFileChooser chooser = new JFileChooser();

        initImageFilter(chooser, true, true, true, true, true);

        int returnVal = chooser.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {

            try {
                m_fileName = chooser.getSelectedFile().getCanonicalPath();

            } catch (IOException ex) {
                Logger.getLogger(MainJFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            return;
        }

        Path fichierValide;

        if (m_fileName != null) {
            fichierValide = Paths.get(m_fileName);

            if (isReadable(fichierValide)) {

                m_reseau.setGraphique(graphiqueSelectionnee, null, null, null, null,
                        null, m_fileName, null, jToggleButton1.isSelected());
                m_fileName = "";
                 m_selectionElement.select(graphiqueSelectionnee, false);
            }
        }
    }//GEN-LAST:event_btnPathActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPath;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JCheckBox jchkDeplacable;
    private javax.swing.JLabel jlbHauteur;
    private javax.swing.JLabel jlbLargeur;
    private javax.swing.JLabel jlbNom;
    private javax.swing.JLabel jlbPath;
    private javax.swing.JLabel jlbX;
    private javax.swing.JLabel jlbY;
    private javax.swing.JSpinner jspHauteur;
    private javax.swing.JSpinner jspLargeur;
    private javax.swing.JSpinner jspX;
    private javax.swing.JSpinner jspY;
    private javax.swing.JTextField jtfNom;
    private javax.swing.JTextField jtfPath;
    // End of variables declaration//GEN-END:variables
}
