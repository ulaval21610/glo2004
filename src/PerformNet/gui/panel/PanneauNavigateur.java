/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet.gui.panel;

import PerformNet.gui.selection.SelectionElement;
import PerformNet.gui.selection.SelectionNulleEvent;
import PerformNet.gui.selection.SelectionNulleListener;
import PerformNet.gui.selection.SelectionStationEvent;
import PerformNet.gui.selection.SelectionStationListener;
import PerformNet.modele.Reseau;
import PerformNet.modele.ReseauModifieEvent;
import PerformNet.modele.ReseauModifieListener;
import PerformNet.modele.reseau.Noeud;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.KeyStroke;

/**
 * classe qui représente une partie du gui visible, permet d'afficher la liste
 * des stations du réseau
 *
 * @author Expresso
 */
public class PanneauNavigateur extends javax.swing.JPanel {

    private static int testVal = 0;
    private static final long serialVersionUID = 5656335556982549488L;
    private transient SelectionElement m_selectionElement;
    private transient Reseau m_reseau;

    private DefaultListModel m_listModel = new DefaultListModel();

    private int[] elementsSelect;
    private List<Integer> arrayIndiceSelect = new ArrayList<>();

    /**
     * Creates new form PanneauNavigateur
     *
     * @param reseau
     * @param elementSelectionne
     */
    public PanneauNavigateur(Reseau reseau, SelectionElement elementSelectionne) {
        m_reseau = reseau;
        m_selectionElement = elementSelectionne;
        updateDefaultListModel();

        initListeners();
        initComponents();
        initAction();

        listStation.putClientProperty("List.isFileList", Boolean.TRUE);
    }

    /*Met à a jour la liste qui contient les stations du réseau
     *
     */
    private void updateDefaultListModel() {
        m_listModel.clear();

        // ajoute les stations du réseau
        List<Noeud> stationsReseau = m_reseau.getStations();
        for (int i = 0; i < m_reseau.getStations().size(); i++) {
            m_listModel.addElement(stationsReseau.get(i));
        }

        highLightStationSelectionne();
    }

    private void getStationSelectionneJlist() {
        List<Noeud> listeSelectModele = m_selectionElement.getStations();
        int index = 0;
        arrayIndiceSelect.clear();

        for (int i = 0; i < m_listModel.size(); i++) {
            if (listeSelectModele.contains((Noeud) m_listModel.get(i))) {
                arrayIndiceSelect.add(index, i);
                index++;
            }
        }

        elementsSelect = new int[arrayIndiceSelect.size()];

        for (int i = 0; i < arrayIndiceSelect.size(); i++) {
            elementsSelect[i] = arrayIndiceSelect.get(i);
        }
    }

    private void highLightStationSelectionne() {
        getStationSelectionneJlist();

        if (elementsSelect.length != 0) {
            listStation.setSelectedIndices(elementsSelect);
        }
    }

    public void mettreAjourStationSelect() {
        if (!listStation.getSelectedValuesList().isEmpty()) {
            m_selectionElement.selectList(listStation.getSelectedValuesList(), true);
        } else {
            m_selectionElement.select(m_selectionElement.getStation(), true);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jScrollPane1 = new javax.swing.JScrollPane();
        listStation = new javax.swing.JList<Noeud>();

        jMenuItem1.setText("Supprimer");
        jPopupMenu1.add(jMenuItem1);

        listStation.setModel(m_listModel);
        listStation.setValueIsAdjusting(true);
        listStation.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listStationMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                listStationMouseReleased(evt);
            }
        });
        listStation.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listStationValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(listStation);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 369, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void listStationMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listStationMouseReleased
        if (evt.isPopupTrigger()) {
            listStation.setSelectedIndex(listStation.locationToIndex(evt.getPoint()));

            jPopupMenu1.show(evt.getComponent(), evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_listStationMouseReleased

    private void listStationMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listStationMouseClicked
        mettreAjourStationSelect();
    }//GEN-LAST:event_listStationMouseClicked

    private void listStationValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listStationValueChanged
        listStation.ensureIndexIsVisible(listStation.getSelectedIndex());
    }//GEN-LAST:event_listStationValueChanged

    private void initListeners() {
        /*Obesrvateur de sélection de station
         *Permet de surligner dans la liste la station sélectionnée dans le planDessin
         *
         */
        m_selectionElement.addSelectionStationListener(new SelectionStationListener() {
            @Override
            public void stationSelectionnee(SelectionStationEvent event) {
                if (event.getStation() != null) {
                    highLightStationSelectionne();
                }
                if (m_selectionElement.getStation() == null) {
                    listStation.clearSelection();
                }
            }
        });

        /*Observateur de désélection de station
         *Permet de délesctionner les éléments de la liste de station lors d'une écoute de non sélection (absence de sélection)
         *
         */
        m_selectionElement.addSelectionNulleListener(new SelectionNulleListener() {

            @Override
            public void selectionNulle(SelectionNulleEvent event) {
                listStation.clearSelection();
                arrayIndiceSelect.clear();
                elementsSelect = new int[0];
            }
        });

        /*observateur de changement du réseau, remet à jour la liste des stations s'il y a changement
         *
         */
        m_reseau.addReseauModifieListener(new ReseauModifieListener() {

            @Override
            public void reseauModifie(ReseauModifieEvent evt) {
                updateDefaultListModel();
            }
        });
    }

    private class DeleteActionImpl extends AbstractAction {

        public DeleteActionImpl() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            if (m_selectionElement.getStation() != null) {
                m_reseau.supprime(m_selectionElement.getStation());
            }
        }
    }

    private void initAction() {
        Action deleteAction;
        deleteAction = new DeleteActionImpl();

        listStation.getInputMap().put(KeyStroke.getKeyStroke("DELETE"), "deleteAction");
        listStation.getActionMap().put("deleteAction", deleteAction);

        jMenuItem1.setAction(deleteAction);
        jMenuItem1.setText("Supprimer");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<Noeud> listStation;
    // End of variables declaration//GEN-END:variables
}
