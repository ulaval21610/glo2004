/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet.gui.container;

import PerformNet.gui.panel.Direction.direction;
import PerformNet.gui.selection.SelectionElement;
import java.awt.Component;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

/**
 *
 * @author Expresso
 */
public class ThreeWayJPanel extends javax.swing.JPanel {
    private static final long serialVersionUID = 1968321295647373010L;

    private SelectionElement m_selectionElement;

    /**
     * Creates new form threePartJpanel
     */
    public ThreeWayJPanel() {
        initComponents();
        initnorthEastJif();
        initsouthEastJif();
        initwestJif();
        jpnWindowButton.setVisible(false);
        jbtnNorthEast.setVisible(false);
        jbtnSouthEast.setVisible(false);
        jpnWindowButton.setVisible(false);
        jbtnNorthEast.setVisible(false);
    }

    public void setPanels(JComponent west, JComponent northEast, JComponent southEast) {
        jScrollPaneWest.setViewportView(west);
        jScrollPaneSouthEast.setViewportView(southEast);

        javax.swing.GroupLayout northEastJifLayout = new javax.swing.GroupLayout(jifNorthEast.getContentPane());
        jifNorthEast.getContentPane().setLayout(northEastJifLayout);
        northEastJifLayout.setHorizontalGroup(
                northEastJifLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(northEast, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
        );
        northEastJifLayout.setVerticalGroup(
                northEastJifLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(northEast, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
        );

        wheelMotionListener(west, jScrollPaneWest);
        wheelMotionListener(northEast, jScrollPaneNorthEast);
        wheelMotionListener(southEast, jScrollPaneSouthEast);
    }

    private void wheelMotionListener(JComponent west, final JScrollPane jsp) {
        west.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            @Override
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                if (evt.getPreciseWheelRotation() > 0) {
                    jsp.getVerticalScrollBar().setValue(jsp.getVerticalScrollBar().getValue() + 5);
                }
                if (evt.getPreciseWheelRotation() < 0) {
                    jsp.getVerticalScrollBar().setValue(jsp.getVerticalScrollBar().getValue() - 5);
                }
            }
        });
    }

    public void invertPane() {
        Component newLeft = jSplitPanePrincipal.getRightComponent();
        Component newRight = jSplitPanePrincipal.getLeftComponent();
        int divLoc = jSplitPanePrincipal.getDividerLocation();

        jSplitPanePrincipal.setRightComponent(null);
        jSplitPanePrincipal.setLeftComponent(null);

        jSplitPanePrincipal.setRightComponent(newRight);
        jSplitPanePrincipal.setLeftComponent(newLeft);

        jSplitPanePrincipal.setDividerLocation(jSplitPanePrincipal.getWidth() - divLoc);
    }

    public void setPanel(JComponent panel, direction panel_position) {
        switch (panel_position) {
            case NORTH_EAST:
                jScrollPaneNorthEast.setViewportView(panel);
                break;
            case SOUTH_EAST:
                jScrollPaneSouthEast.setViewportView(panel);
                break;
            case WEST:
                jScrollPaneWest.setViewportView(panel);
                break;
        }
    }

    public void setFramName(direction d, String title) {

        switch (d) {
            case WEST:
                jifWest.setTitle(title);
                break;
            case NORTH_EAST:
                jifNorthEast.setTitle(title);
                break;
            case SOUTH_EAST:
                jifSouthEast.setTitle(title);
                break;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpnWindowButton = new javax.swing.JPanel();
        jbtnNorthEast = new javax.swing.JButton();
        jbtnSouthEast = new javax.swing.JButton();
        jSplitPanePrincipal = new javax.swing.JSplitPane();
        jSplitPaneSub = new javax.swing.JSplitPane();
        jifNorthEast = new javax.swing.JInternalFrame();
        jScrollPaneNorthEast = new javax.swing.JScrollPane();
        jifSouthEast = new javax.swing.JInternalFrame();
        jScrollPaneSouthEast = new javax.swing.JScrollPane();
        jifWest = new javax.swing.JInternalFrame();
        jScrollPaneWest = new javax.swing.JScrollPane();

        setLayout(new java.awt.BorderLayout());

        jpnWindowButton.setMinimumSize(new java.awt.Dimension(50, 100));
        jpnWindowButton.setPreferredSize(new java.awt.Dimension(50, 100));
        jpnWindowButton.setLayout(new javax.swing.BoxLayout(jpnWindowButton, javax.swing.BoxLayout.Y_AXIS));

        jbtnNorthEast.setText("jButton1");
        jbtnNorthEast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnNorthEastActionPerformed(evt);
            }
        });
        jpnWindowButton.add(jbtnNorthEast);

        jbtnSouthEast.setText("jButton2");
        jpnWindowButton.add(jbtnSouthEast);

        add(jpnWindowButton, java.awt.BorderLayout.EAST);

        jSplitPanePrincipal.setDividerLocation(775);
        jSplitPanePrincipal.setResizeWeight(1.0);
        jSplitPanePrincipal.setContinuousLayout(true);
        jSplitPanePrincipal.setOneTouchExpandable(true);

        jSplitPaneSub.setDividerLocation(180);
        jSplitPaneSub.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPaneSub.setResizeWeight(1.0);
        jSplitPaneSub.setContinuousLayout(true);
        jSplitPaneSub.setOneTouchExpandable(true);

        jifNorthEast.setBorder(null);
        jifNorthEast.setVisible(true);

        javax.swing.GroupLayout jifNorthEastLayout = new javax.swing.GroupLayout(jifNorthEast.getContentPane());
        jifNorthEast.getContentPane().setLayout(jifNorthEastLayout);
        jifNorthEastLayout.setHorizontalGroup(
            jifNorthEastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 79, Short.MAX_VALUE)
            .addGroup(jifNorthEastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jifNorthEastLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPaneNorthEast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jifNorthEastLayout.setVerticalGroup(
            jifNorthEastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jifNorthEastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jifNorthEastLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPaneNorthEast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jSplitPaneSub.setTopComponent(jifNorthEast);
        jifNorthEast.setFrameIcon(null);

        jifSouthEast.setBorder(null);
        jifSouthEast.setVisible(true);

        javax.swing.GroupLayout jifSouthEastLayout = new javax.swing.GroupLayout(jifSouthEast.getContentPane());
        jifSouthEast.getContentPane().setLayout(jifSouthEastLayout);
        jifSouthEastLayout.setHorizontalGroup(
            jifSouthEastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPaneSouthEast, javax.swing.GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE)
        );
        jifSouthEastLayout.setVerticalGroup(
            jifSouthEastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPaneSouthEast, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
        );

        jSplitPaneSub.setRightComponent(jifSouthEast);
        jifSouthEast.setFrameIcon(null);

        jSplitPanePrincipal.setRightComponent(jSplitPaneSub);

        jifWest.setBorder(null);
        jifWest.setVisible(true);

        javax.swing.GroupLayout jifWestLayout = new javax.swing.GroupLayout(jifWest.getContentPane());
        jifWest.getContentPane().setLayout(jifWestLayout);
        jifWestLayout.setHorizontalGroup(
            jifWestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPaneWest, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 770, Short.MAX_VALUE)
        );
        jifWestLayout.setVerticalGroup(
            jifWestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPaneWest, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 453, Short.MAX_VALUE)
        );

        jSplitPanePrincipal.setLeftComponent(jifWest);
        jifWest.setFrameIcon(null);

        add(jSplitPanePrincipal, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jbtnNorthEastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnNorthEastActionPerformed
        // jpnWindowButton.setVisible(false);
        jifNorthEast.setVisible(false);
    }//GEN-LAST:event_jbtnNorthEastActionPerformed

    private void initnorthEastJif() {
        jifNorthEast.addInternalFrameListener(new InternalFrameListener() {

            @Override
            public void internalFrameOpened(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameClosing(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameClosed(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameIconified(InternalFrameEvent e) {
                jSplitPaneSub.setDividerLocation(0);
            }

            @Override
            public void internalFrameDeiconified(InternalFrameEvent e) {
                jSplitPanePrincipal.setDividerLocation(0);
                jSplitPaneSub.setDividerLocation(jSplitPanePrincipal.getWidth());
            }

            @Override
            public void internalFrameActivated(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameDeactivated(InternalFrameEvent e) {
            }
        });

    }

    private void initsouthEastJif() {
        jifSouthEast.addInternalFrameListener(new InternalFrameListener() {

            @Override
            public void internalFrameOpened(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameClosing(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameClosed(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameIconified(InternalFrameEvent e) {
                jSplitPaneSub.setDividerLocation(jSplitPanePrincipal.getWidth());
                // jpnWindowButton.setVisible(true);
                // jbtnSouthEast.setVisible(true);
            }

            @Override
            public void internalFrameDeiconified(InternalFrameEvent e) {
                jSplitPanePrincipal.setDividerLocation(0);
                jSplitPaneSub.setDividerLocation(0);
            }

            @Override
            public void internalFrameActivated(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameDeactivated(InternalFrameEvent e) {
            }
        });

    }

    private void initwestJif() {
        jifWest.addInternalFrameListener(new InternalFrameListener() {

            @Override
            public void internalFrameOpened(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameClosing(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameClosed(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameIconified(InternalFrameEvent e) {
                jSplitPanePrincipal.setDividerLocation(0);
            }

            @Override
            public void internalFrameDeiconified(InternalFrameEvent e) {
                jSplitPanePrincipal.setDividerLocation(jSplitPanePrincipal.getWidth() + jSplitPaneSub.getWidth());
            }

            @Override
            public void internalFrameActivated(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameDeactivated(InternalFrameEvent e) {
            }
        });

    }

    public void addPanel(JComponent jp) {
        jScrollPaneWest.setViewportView(jp);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPaneNorthEast;
    private javax.swing.JScrollPane jScrollPaneSouthEast;
    private javax.swing.JScrollPane jScrollPaneWest;
    private javax.swing.JSplitPane jSplitPanePrincipal;
    private javax.swing.JSplitPane jSplitPaneSub;
    private javax.swing.JButton jbtnNorthEast;
    private javax.swing.JButton jbtnSouthEast;
    private javax.swing.JInternalFrame jifNorthEast;
    private javax.swing.JInternalFrame jifSouthEast;
    private javax.swing.JInternalFrame jifWest;
    private javax.swing.JPanel jpnWindowButton;
    // End of variables declaration//GEN-END:variables
}
