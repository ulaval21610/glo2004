/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet.gui;

import PerformNet.gui.action.Enregistrer;
import PerformNet.gui.action.EnregistrerSous;
import PerformNet.gui.action.ExporterImage;
import PerformNet.gui.action.ImporterImageWindow;
import PerformNet.gui.action.InsererArc;
import PerformNet.gui.action.InsererImage;
import PerformNet.gui.action.InsererStation;
import PerformNet.gui.action.OuvertureFichier;
import PerformNet.gui.action.PasserArriere;
import PerformNet.gui.action.PasserAvant;
import PerformNet.gui.action.Redo;
import PerformNet.gui.action.Selection;
import PerformNet.gui.action.Undo;
import static PerformNet.gui.panel.Direction.direction.EAST;
import static PerformNet.gui.panel.Direction.direction.NORTH_EAST;
import static PerformNet.gui.panel.Direction.direction.SOUTH_EAST;
import PerformNet.gui.panel.PanneauNavigateur;
import PerformNet.gui.panel.PanneauProprietesImage;
import PerformNet.gui.panel.PanneauProprietesReseau;
import PerformNet.gui.panel.PanneauProprietesStation;
import PerformNet.gui.plan.PlanDessin;
import PerformNet.gui.selection.SelectionArcEvent;
import PerformNet.gui.selection.SelectionArcListener;
import PerformNet.gui.selection.SelectionElement;
import PerformNet.gui.selection.SelectionFlexEvent;
import PerformNet.gui.selection.SelectionFlexListener;
import PerformNet.gui.selection.SelectionGraphiqueEvent;
import PerformNet.gui.selection.SelectionGraphiqueListener;
import PerformNet.gui.selection.SelectionNulleEvent;
import PerformNet.gui.selection.SelectionNulleListener;
import PerformNet.gui.selection.SelectionStationEvent;
import PerformNet.gui.selection.SelectionStationListener;
import PerformNet.modele.Reseau;
import PerformNet.modele.ReseauModifieEvent;
import PerformNet.modele.ReseauModifieListener;
import PerformNet.modele.reseau.Arc;
import PerformNet.modele.reseau.Noeud;
import java.awt.AWTException;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;

/**
 * Classe représentant l'interface jframe gui du programme, instancie l'objet
 * planDessin qui gère le rendu du plan et la captation des interactions
 * utilisateur, instancie les objets jpanel sous section du gui (Panneau
 * Proprietes Station, Panneau Proprietes Reseau et Panneau Navigateur)
 *
 * @author Expresso
 */
public class MainJFrame extends javax.swing.JFrame {

    private static final long serialVersionUID = -6427281481993962991L;

    private final ButtonGroup typeAction = new ButtonGroup();
    private transient Reseau m_reseau;
    private final Container contentPane;

    private transient Action loading;
    private transient Action enregistrerSous;
    private transient Action enregistrer;
    private transient Action exporter;
    private transient Action importerImage;
    private transient Action selection;
    private transient Action insererArc;
    private transient Action insererStation;
    private transient Action insererImage;
    private transient Action undo;
    private transient Action redo;

    private transient SelectionElement m_selectionElement = new SelectionElement();
    private PanneauProprietesStation m_panneauStation;
    private PanneauProprietesReseau m_panneauPropReseau;
    private PanneauNavigateur m_panneauNavigateur;
    private PanneauProprietesImage m_panneauImage;

    private transient final UserPrefs pref = UserPrefs.getInstance();

    /**
     *
     */
    public MainJFrame() {
        this.contentPane = this.getContentPane();
        initComponents();
        this.setLocation((java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().width / 2) - (this.getWidth() / 2), (java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height / 2) - (this.getHeight() / 2));
    }

    /*
     *crée nouvelle instance de MainJFrame qui instancie les trois sous section du gui 
     *(Panneau Proprietes Station, Panneau Proprietes Reseau et Panneau Navigateur).
     *Des observateurs affichent ces sous-sections selon l'objet courant sélectionné dans le plan m_planDessin
     * @param reseau
     */
    public MainJFrame(Reseau reseau) {
        this.contentPane = this.getContentPane();
        m_reseau = reseau;
        m_panneauStation = new PanneauProprietesStation(m_reseau, m_selectionElement);
        m_panneauNavigateur = new PanneauNavigateur(m_reseau, m_selectionElement);
        m_panneauPropReseau = new PanneauProprietesReseau(m_reseau);
        m_panneauImage = new PanneauProprietesImage(m_reseau, m_selectionElement);

        setIcon();
        initComponents();
        initActions();
        initEscapeAction();
        initListeners();

        objetContextuel(false);

        threeWayJPanel1.setPanels(m_planDessin, m_panneauNavigateur, m_panneauPropReseau);
        threeWayJPanel1.setFramName(NORTH_EAST, "Sélection");
        threeWayJPanel1.setFramName(SOUTH_EAST, "Propriétés - Réseau");

        // temporaire -- TODO listener état
        jtBnSelection.setSelected(false);
        jtBnSelection.setSelected(true);

        // constructor leaking -- TODO Fix
        pref.loadPreference(this, m_reseau, jPanelAffichage1, threeWayJPanel1);
    }

    /**
     * Instancie des objets du package PerformNet.gui.action en associant une
     * action (ouverture de fenêtres de sauvegarde de projet, de chargement de
     * projet, de chargement d'image, d'insertion de station, et d'arc et de
     * création d'objet sélection) à un bouton à laquelle cette action
     * correspond.
     *
     */
    private void initActions() {

        enregistrerSous = new EnregistrerSous(this, m_reseau);//instancie une fenêtre de sauvegarde de ficher
        enregistrer = new Enregistrer(this, m_reseau);//instancie une fenêtre de sauvegarde de ficher
        loading = new OuvertureFichier(this, m_reseau, m_selectionElement);//instancie une fenêtre d'ouverture de fichier
        importerImage = new ImporterImageWindow(this, m_planDessin);//instancie une fenêtre d'importation d'image de fond
        selection = new Selection(m_planDessin, jtBnSelection);//instancie un objet sélection 
        insererStation = new InsererStation(m_planDessin, jtBtnInsererStation);//instancie un objet d'insertiom de station

        insererArc = new InsererArc(m_planDessin, jtBnInsererArc);//instanice un objet d'insertion d'Arc
        insererImage = new InsererImage(this, m_planDessin);//instancie un objet d'insertion d'image
        exporter = new ExporterImage(this, m_planDessin);

        Action passerAvant = new PasserAvant(m_selectionElement, m_reseau);
        Action passerArriere = new PasserArriere(m_selectionElement, m_reseau);

        redo = new Redo(m_reseau, m_selectionElement);
        undo = new Undo(m_reseau, m_selectionElement);

        // menu
        jMenuItem_EnregistrerSous.setAction(enregistrerSous);
        jMenuItem_Enregistrer.setAction(enregistrer);
        jMenuItem_Ouvrir.setAction(loading);
        jMenuItem_Importer.setAction(importerImage);
        jMenuItem_PasserAvant.setAction(passerAvant);
        jMenuItem_PasserArriere.setAction(passerArriere);
        jMenuItem_Retablir.setAction(redo);
        jMenuItem_Annuler.setAction(undo);
        jMenuItem_Exporter.setAction(exporter);

        // toolbar
        jtBtnInsererStation.setAction(insererStation);
        jtBtnInsererStation.setText("");
        jtBnInsererArc.setAction(insererArc);
        jtBnInsererArc.setText("");
        btnInsererImage.setAction(insererImage);
        btnInsererImage.setText("");
        jtBnSelection.setAction(selection);
        jtBnSelection.setText("");

        // context menu
        jmiSelection.setAction(selection);
        jmiInsererStation.setAction(insererStation);
        jmiInsererArc.setAction(insererArc);
        jmiInsererImage.setAction(insererImage);
        jmiPasseAvant.setAction(passerAvant);
        jmiPasserArriere.setAction(passerArriere);

        //jsplitButton
        jpBtnUndo.setButtonAction(undo);
        jpBtnRedo.setButtonAction(redo);
    }

    private void initListeners() {
        /*  ajoute un observateur qui écoute l'objet station sélectionnée.  
         *  Si une station est sélectionnée, affiche "Propriétés" + son nom ainsi que le panneau Propriétés Station 
         *  dans la fraction sud est du jpanel threeWayJPanel1
         *  qui représente l'arangement des sous fenêtres du mainjframe
         *  Si aucune station est sélectionné, ne fait qu'afficher le mot "Propriétés" dans le PanneauProprietesStation 
         */
        m_selectionElement.addSelectionStationListener(new SelectionStationListener() {
            @Override
            public void stationSelectionnee(SelectionStationEvent event) {
                //Multi sélection
                if (m_selectionElement.getStations().size() > 1) {
                    threeWayJPanel1.setFramName(SOUTH_EAST, "Propriétés - Multisélection");
                    threeWayJPanel1.setPanel(jPanel_empty, SOUTH_EAST);
                    return;
                }

                //Station unique
                threeWayJPanel1.setPanel(m_panneauStation, SOUTH_EAST);
                threeWayJPanel1.setFramName(SOUTH_EAST, "Propriétés - " + event.getStation().getNom());
                m_panneauStation.setStation(event.getStation(), null);

                objetContextuel(true);
            }
        });

        m_selectionElement.addSelectionArcListener(new SelectionArcListener() {

            @Override
            public void arcSelectionnee(SelectionArcEvent event) {
                //Multi sélection
                if (m_selectionElement.getStations().size() > 1) {
                    threeWayJPanel1.setFramName(SOUTH_EAST, "Propriétés - Multisélection");
                    threeWayJPanel1.setPanel(jPanel_empty, SOUTH_EAST);
                    return;
                }

                //Station unique
                Noeud station = event.getArc().getOrigine();
                threeWayJPanel1.setPanel(m_panneauStation, SOUTH_EAST);
                threeWayJPanel1.setFramName(SOUTH_EAST, "Propriétés - " + station + " - Arc "
                                            + (station.getArcOutIndexOf(event.getArc()) + 1));
                m_panneauStation.setStation(station, event.getArc());

                objetContextuel(true);
            }
        });

        /*ajoute un observateur de l'objet SelectionNulleListener, qui représente l'absence de sélection d'un élémnet 
         *du plan du réseau (l'absence de sélection est activée quand on clic partout dans le plan du réseau sauf sur une station)
         *si la "non sélection" est activée, affiche les propriétés du réseau avec l'objet jpanel m_panneauPropReseau dans la fraction sud est 
         *du threeWayJPanel1
         */
        m_selectionElement.addSelectionNulleListener(new SelectionNulleListener() {
            @Override
            public void selectionNulle(SelectionNulleEvent event) {
                threeWayJPanel1.setPanel(m_panneauPropReseau, SOUTH_EAST);
                threeWayJPanel1.setFramName(SOUTH_EAST, "Propriétés - Réseau");

                objetContextuel(false);
            }
        });

        m_selectionElement.addSelectionGraphiqueListener(new SelectionGraphiqueListener() {

            @Override
            public void graphiqueSelectionne(SelectionGraphiqueEvent event) {
                threeWayJPanel1.setPanel(m_panneauImage, SOUTH_EAST);
                threeWayJPanel1.setFramName(SOUTH_EAST, "Propriétés - Image");

                if (m_selectionElement.getStations().size() > 1
                        || m_selectionElement.getGraphiques().size() > 1
                        || (m_selectionElement.getStations().size() >= 1
                        && m_selectionElement.getGraphiques().size() >= 1)) {
                    threeWayJPanel1.setFramName(SOUTH_EAST, "Propriétés - Multisélection");
                    threeWayJPanel1.setPanel(jPanel_empty, SOUTH_EAST);
                }

                objetContextuel(true);
            }
        });

        m_selectionElement.addSelectionFlexListener(new SelectionFlexListener() {

            @Override
            public void flexSelectionne(SelectionFlexEvent event) {
                objetContextuel(true);
            }
        });

        m_reseau.addReseauModifieListener(new ReseauModifieListener() {

            @Override
            public void reseauModifie(ReseauModifieEvent evt) {
                if (m_selectionElement.getStation() != null) {

                    threeWayJPanel1.setFramName(SOUTH_EAST, "Propriétés - " + m_selectionElement.getStation().getNom());

                    if (m_selectionElement.getStations().size() > 1
                            || m_selectionElement.getGraphiques().size() > 1
                            || (m_selectionElement.getStations().size() >= 1
                            && m_selectionElement.getGraphiques().size() >= 1)) {
                        threeWayJPanel1.setFramName(SOUTH_EAST, "Propriétés - Multisélection");
                        threeWayJPanel1.setPanel(jPanel_empty, SOUTH_EAST);
                    }
                }
                jMenuItem_EffacerFond.setEnabled(m_reseau.isArrierePlan());

                jpBtnUndo.jButton1.setEnabled(m_reseau.isUndo());
                undo.setEnabled(m_reseau.isUndo());
                jpBtnRedo.jButton1.setEnabled(m_reseau.isRedo());
                redo.setEnabled(m_reseau.isRedo());

                jchk_ImageFond.setEnabled(m_reseau.isArrierePlan());
            }
        });
    }

    private void objetContextuel(boolean etat) {
        jMenuItem_PasserArriere.setEnabled(etat);
        jMenuItem_PasserAvant.setEnabled(etat);
        jMenuItem_Effacer.setEnabled(etat);
    }

    void setBoiteOutil(boolean visible) {
        jtbBoiteOutil.setVisible(visible);
    }

    private void deleteEvent() {
        Robot robot;
        try {
            robot = new Robot();
            robot.keyPress(KeyEvent.VK_DELETE);
        } catch (AWTException ex) {
            Logger.getLogger(MainJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/img/icones_menu/performnet.png")));
    }

    private void closeConfirmation() {
        Action temp;
        if (jMenuItem_Enregistrer.isEnabled()) {
            temp = enregistrer;
        } else {
            temp = enregistrerSous;
        }
        JopConfirmation jopc = new JopConfirmation(this, true, temp);
        jopc.setAlwaysOnTop(false);

        jopc.setVisible(true);
        if (jopc.getState() == 2) {
            System.exit(0);
        }
    }

    private void initEscapeAction() {
        Action escape;
        escape = new EscapeActionImpl();

        m_planDessin.getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "escape");
        m_planDessin.getActionMap().put("escape", escape);
    }

    private class EscapeActionImpl extends AbstractAction {

        public EscapeActionImpl() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            jtBnSelection.setSelected(true);
            m_planDessin.changeEtat(PlanDessin.Etat.SELECTION, true);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel_empty = new javax.swing.JPanel();
        jpmPlanDessin = new javax.swing.JPopupMenu();
        jmiSelection = new javax.swing.JMenuItem();
        jSeparator11 = new javax.swing.JPopupMenu.Separator();
        jmiInsererStation = new javax.swing.JMenuItem();
        jmiInsererArc = new javax.swing.JMenuItem();
        jmiInsererImage = new javax.swing.JMenuItem();
        jSeparator10 = new javax.swing.JPopupMenu.Separator();
        jmiSupprimer = new javax.swing.JMenuItem();
        jSeparator13 = new javax.swing.JPopupMenu.Separator();
        jmiPasseAvant = new javax.swing.JMenuItem();
        jmiPasserArriere = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        jCheckBoxMenuItem_Activer = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem_Afficher = new javax.swing.JCheckBoxMenuItem();
        jMenuItem_Zoom = new javax.swing.JMenuItem();
        m_planDessin = new PerformNet.gui.plan.PlanDessin(m_reseau, m_selectionElement);
        jmTaillePolice = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem_Couper = new javax.swing.JMenuItem();
        jMenuItem_Copier = new javax.swing.JMenuItem();
        jMenuItem_Coller = new javax.swing.JMenuItem();
        jSeparator9 = new javax.swing.JPopupMenu.Separator();
        threeWayJPanel1 = new PerformNet.gui.container.ThreeWayJPanel();
        jtbBoiteOutil = new javax.swing.JToolBar();
        jPanel1 = new javax.swing.JPanel();
        jtBnSelection = new javax.swing.JToggleButton();
        jtBtnInsererStation = new javax.swing.JToggleButton();
        jtBnInsererArc = new javax.swing.JToggleButton();
        btnInsererImage = new javax.swing.JButton();
        jpBtnUndo = new PerformNet.gui.control.JSplitButton(EAST);
        jpBtnRedo = new PerformNet.gui.control.JSplitButton(EAST);
        jPanelAffichage1 = new PerformNet.gui.panel.PanneauProprietesAffichage(m_reseau, m_planDessin);
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu_Fichier = new javax.swing.JMenu();
        jMenuItem_Nouveau = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        jMenuItem_Ouvrir = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        jMenuItem_Importer = new javax.swing.JMenuItem();
        jMenuItem_EffacerFond = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenuItem_Enregistrer = new javax.swing.JMenuItem();
        jMenuItem_EnregistrerSous = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        jMenuItem_Exporter = new javax.swing.JMenuItem();
        jMenuItem_Imprimer = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem_Quitter = new javax.swing.JMenuItem();
        jMenu_Edition = new javax.swing.JMenu();
        jMenuItem_Annuler = new javax.swing.JMenuItem();
        jMenuItem_Retablir = new javax.swing.JMenuItem();
        jSeparator7 = new javax.swing.JPopupMenu.Separator();
        jMenuItem_Effacer = new javax.swing.JMenuItem();
        jMenuItem_PasserArriere = new javax.swing.JMenuItem();
        jMenuItem_PasserAvant = new javax.swing.JMenuItem();
        jmiAlign = new javax.swing.JMenuItem();
        jMenuItem_SelectionnerTout = new javax.swing.JMenuItem();
        jMenu_Affichage = new javax.swing.JMenu();
        jCheckBoxMenuItem_AfficherBarreOutil = new javax.swing.JCheckBoxMenuItem();
        jmiInverserVue = new javax.swing.JMenuItem();
        jSeparator12 = new javax.swing.JPopupMenu.Separator();
        jchk_ImageFond = new javax.swing.JCheckBoxMenuItem();
        jSeparator8 = new javax.swing.JPopupMenu.Separator();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator14 = new javax.swing.JPopupMenu.Separator();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem4 = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem2 = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem3 = new javax.swing.JCheckBoxMenuItem();
        jMenu_Option = new javax.swing.JMenu();
        jMenuItem_Preference = new javax.swing.JMenuItem();
        jMenu_Aide = new javax.swing.JMenu();
        jMenuItem_Aide = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        jMenuItem_Apropos = new javax.swing.JMenuItem();

        javax.swing.GroupLayout jPanel_emptyLayout = new javax.swing.GroupLayout(jPanel_empty);
        jPanel_empty.setLayout(jPanel_emptyLayout);
        jPanel_emptyLayout.setHorizontalGroup(
            jPanel_emptyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel_emptyLayout.setVerticalGroup(
            jPanel_emptyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jmiSelection.setText("Arc");
        jpmPlanDessin.add(jmiSelection);
        jpmPlanDessin.add(jSeparator11);

        jmiInsererStation.setText("Arc");
        jpmPlanDessin.add(jmiInsererStation);

        jmiInsererArc.setText("jMenuItem1");
        jpmPlanDessin.add(jmiInsererArc);

        jmiInsererImage.setText("Arc");
        jpmPlanDessin.add(jmiInsererImage);
        jpmPlanDessin.add(jSeparator10);

        jmiSupprimer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/icones_menu/delete.png"))); // NOI18N
        jmiSupprimer.setText("Supprimer");
        jmiSupprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiSupprimerActionPerformed(evt);
            }
        });
        jpmPlanDessin.add(jmiSupprimer);
        jpmPlanDessin.add(jSeparator13);

        jmiPasseAvant.setText("jMenuItem12");
        jpmPlanDessin.add(jmiPasseAvant);

        jmiPasserArriere.setText("jMenuItem13");
        jpmPlanDessin.add(jmiPasserArriere);

        jMenu6.setText("Grille magnétique");

        jCheckBoxMenuItem_Activer.setSelected(true);
        jCheckBoxMenuItem_Activer.setText("Activer");
        jMenu6.add(jCheckBoxMenuItem_Activer);

        jCheckBoxMenuItem_Afficher.setSelected(true);
        jCheckBoxMenuItem_Afficher.setText("Afficher");
        jMenu6.add(jCheckBoxMenuItem_Afficher);

        jMenuItem_Zoom.setText("Zoom");

        m_planDessin.setBackground(new java.awt.Color(80, 80, 80));
        m_planDessin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                m_planDessinMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout m_planDessinLayout = new javax.swing.GroupLayout(m_planDessin);
        m_planDessin.setLayout(m_planDessinLayout);
        m_planDessinLayout.setHorizontalGroup(
            m_planDessinLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        m_planDessinLayout.setVerticalGroup(
            m_planDessinLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 602, Short.MAX_VALUE)
        );

        jmTaillePolice.setText("Taille Police");

        jMenuItem3.setText("8 pt");
        jmTaillePolice.add(jMenuItem3);

        jMenuItem4.setText("9 pt");
        jmTaillePolice.add(jMenuItem4);

        jMenuItem5.setText("jMenuItem3");
        jmTaillePolice.add(jMenuItem5);

        jMenuItem6.setText("jMenuItem3");
        jmTaillePolice.add(jMenuItem6);

        jMenuItem7.setText("jMenuItem3");
        jmTaillePolice.add(jMenuItem7);

        jMenuItem8.setText("jMenuItem3");
        jmTaillePolice.add(jMenuItem8);

        jMenuItem9.setText("jMenuItem3");
        jmTaillePolice.add(jMenuItem9);

        jMenuItem10.setText("jMenuItem3");
        jmTaillePolice.add(jMenuItem10);

        jMenuItem11.setText("jMenuItem11");

        jMenuItem_Couper.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem_Couper.setText("Couper");
        jMenuItem_Couper.setEnabled(false);

        jMenuItem_Copier.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem_Copier.setText("Copier");
        jMenuItem_Copier.setEnabled(false);

        jMenuItem_Coller.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem_Coller.setText("Coller");
        jMenuItem_Coller.setEnabled(false);

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("Ressources/Ressources"); // NOI18N
        setTitle(bundle.getString("NomApplication") ); // NOI18N
        setPreferredSize(new java.awt.Dimension(1200, 770));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().add(threeWayJPanel1, java.awt.BorderLayout.CENTER);

        jtbBoiteOutil.setRollover(true);
        jtbBoiteOutil.setDoubleBuffered(true);
        jtbBoiteOutil.setPreferredSize(new java.awt.Dimension(80, 51));
        jtbBoiteOutil.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jtbBoiteOutilMouseDragged(evt);
            }
        });
        jtbBoiteOutil.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jtbBoiteOutilPropertyChange(evt);
            }
        });

        jPanel1.setMaximumSize(new java.awt.Dimension(190, 49));
        jPanel1.setMinimumSize(new java.awt.Dimension(190, 49));
        jPanel1.setPreferredSize(new java.awt.Dimension(190, 49));
        jPanel1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 2, 5));

        jtBnSelection.setFocusable(false);
        jtBnSelection.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jtBnSelection.setMaximumSize(new java.awt.Dimension(45, 41));
        jtBnSelection.setPreferredSize(new java.awt.Dimension(45, 41));
        jtBnSelection.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jPanel1.add(jtBnSelection);
        typeAction.add(jtBnSelection);

        jtBtnInsererStation.setFocusable(false);
        jtBtnInsererStation.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jtBtnInsererStation.setMaximumSize(new java.awt.Dimension(45, 41));
        jtBtnInsererStation.setPreferredSize(new java.awt.Dimension(45, 41));
        jtBtnInsererStation.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jPanel1.add(jtBtnInsererStation);
        typeAction.add(jtBtnInsererStation);

        jtBnInsererArc.setFocusable(false);
        jtBnInsererArc.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jtBnInsererArc.setMaximumSize(new java.awt.Dimension(45, 41));
        jtBnInsererArc.setPreferredSize(new java.awt.Dimension(45, 41));
        jtBnInsererArc.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jPanel1.add(jtBnInsererArc);
        typeAction.add(jtBnInsererArc);

        btnInsererImage.setFocusable(false);
        btnInsererImage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnInsererImage.setMaximumSize(new java.awt.Dimension(45, 41));
        btnInsererImage.setMinimumSize(new java.awt.Dimension(45, 41));
        btnInsererImage.setPreferredSize(new java.awt.Dimension(45, 41));
        btnInsererImage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jPanel1.add(btnInsererImage);

        jtbBoiteOutil.add(jPanel1);

        jpBtnUndo.setForeground(new java.awt.Color(255, 51, 51));
        jpBtnUndo.setMaximumSize(new java.awt.Dimension(55, 41));
        jpBtnUndo.setMinimumSize(new java.awt.Dimension(55, 41));
        jpBtnUndo.setPreferredSize(new java.awt.Dimension(55, 41));
        jtbBoiteOutil.add(jpBtnUndo);

        jpBtnRedo.setMaximumSize(new java.awt.Dimension(55, 41));
        jpBtnRedo.setMinimumSize(new java.awt.Dimension(55, 41));
        jpBtnRedo.setPreferredSize(new java.awt.Dimension(55, 41));
        jtbBoiteOutil.add(jpBtnRedo);

        getContentPane().add(jtbBoiteOutil, java.awt.BorderLayout.NORTH);
        getContentPane().add(jPanelAffichage1, java.awt.BorderLayout.SOUTH);

        jMenuBar1.setDoubleBuffered(true);

        jMenu_Fichier.setMnemonic('f');
        jMenu_Fichier.setText("Fichier");

        jMenuItem_Nouveau.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem_Nouveau.setText("Nouveau");
        jMenuItem_Nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_NouveauActionPerformed(evt);
            }
        });
        jMenu_Fichier.add(jMenuItem_Nouveau);
        jMenu_Fichier.add(jSeparator4);

        jMenuItem_Ouvrir.setText("Ouvrir");
        jMenu_Fichier.add(jMenuItem_Ouvrir);
        jMenu_Fichier.add(jSeparator5);

        jMenuItem_Importer.setText("Importer");
        jMenu_Fichier.add(jMenuItem_Importer);

        jMenuItem_EffacerFond.setText("Supprimer Image de fond");
        jMenuItem_EffacerFond.setEnabled(false);
        jMenuItem_EffacerFond.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_EffacerFondActionPerformed(evt);
            }
        });
        jMenu_Fichier.add(jMenuItem_EffacerFond);
        jMenu_Fichier.add(jSeparator2);

        jMenuItem_Enregistrer.setText("Enregistrer");
        jMenuItem_Enregistrer.setEnabled(false);
        jMenu_Fichier.add(jMenuItem_Enregistrer);

        jMenuItem_EnregistrerSous.setText("Enregistrer sous...");
        jMenuItem_EnregistrerSous.setEnabled(false);
        jMenu_Fichier.add(jMenuItem_EnregistrerSous);
        jMenu_Fichier.add(jSeparator6);

        jMenuItem_Exporter.setText("Exporter");
        jMenu_Fichier.add(jMenuItem_Exporter);

        jMenuItem_Imprimer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem_Imprimer.setText("Imprimer");
        jMenuItem_Imprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_ImprimerActionPerformed(evt);
            }
        });
        jMenu_Fichier.add(jMenuItem_Imprimer);
        jMenu_Fichier.add(jSeparator1);

        jMenuItem_Quitter.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem_Quitter.setText("Quitter");
        jMenuItem_Quitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_QuitterActionPerformed(evt);
            }
        });
        jMenu_Fichier.add(jMenuItem_Quitter);

        jMenuBar1.add(jMenu_Fichier);

        jMenu_Edition.setMnemonic('d');
        jMenu_Edition.setText("Edition");

        jMenuItem_Annuler.setText("Annuler");
        jMenu_Edition.add(jMenuItem_Annuler);

        jMenuItem_Retablir.setText("Rétablir");
        jMenu_Edition.add(jMenuItem_Retablir);
        jMenu_Edition.add(jSeparator7);

        jMenuItem_Effacer.setText("Supprimer");
        jMenuItem_Effacer.setEnabled(false);
        jMenuItem_Effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_EffacerActionPerformed(evt);
            }
        });
        jMenu_Edition.add(jMenuItem_Effacer);

        jMenuItem_PasserArriere.setText("Passer à l'arrière");
        jMenuItem_PasserArriere.setEnabled(false);
        jMenu_Edition.add(jMenuItem_PasserArriere);

        jMenuItem_PasserAvant.setText("Passer à l'avant");
        jMenuItem_PasserAvant.setEnabled(false);
        jMenu_Edition.add(jMenuItem_PasserAvant);

        jmiAlign.setText("Aligner…");
        jmiAlign.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiAlignActionPerformed(evt);
            }
        });
        jMenu_Edition.add(jmiAlign);

        jMenuItem_SelectionnerTout.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem_SelectionnerTout.setText("Sélectionner tout");
        jMenuItem_SelectionnerTout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_SelectionnerToutActionPerformed(evt);
            }
        });
        jMenu_Edition.add(jMenuItem_SelectionnerTout);

        jMenuBar1.add(jMenu_Edition);

        jMenu_Affichage.setMnemonic('c');
        jMenu_Affichage.setText("Affichage");

        jCheckBoxMenuItem_AfficherBarreOutil.setSelected(true);
        jCheckBoxMenuItem_AfficherBarreOutil.setText("Afficher barre d'outils");
        jCheckBoxMenuItem_AfficherBarreOutil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem_AfficherBarreOutilActionPerformed(evt);
            }
        });
        jMenu_Affichage.add(jCheckBoxMenuItem_AfficherBarreOutil);

        jmiInverserVue.setText("Inverser vue");
        jmiInverserVue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiInverserVueActionPerformed(evt);
            }
        });
        jMenu_Affichage.add(jmiInverserVue);
        jMenu_Affichage.add(jSeparator12);

        jchk_ImageFond.setSelected(true);
        jchk_ImageFond.setText("Afficher l'image de fond");
        jchk_ImageFond.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jchk_ImageFondActionPerformed(evt);
            }
        });
        jMenu_Affichage.add(jchk_ImageFond);
        jMenu_Affichage.add(jSeparator8);

        jMenuItem1.setText("Police réduire");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu_Affichage.add(jMenuItem1);

        jMenuItem2.setText("Police augmenter");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu_Affichage.add(jMenuItem2);
        jMenu_Affichage.add(jSeparator14);

        jCheckBoxMenuItem1.setSelected(true);
        jCheckBoxMenuItem1.setText("Filtre Station");
        jCheckBoxMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem1ActionPerformed(evt);
            }
        });
        jMenu_Affichage.add(jCheckBoxMenuItem1);

        jCheckBoxMenuItem4.setSelected(true);
        jCheckBoxMenuItem4.setText("Filtre Arc");
        jCheckBoxMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem4ActionPerformed(evt);
            }
        });
        jMenu_Affichage.add(jCheckBoxMenuItem4);

        jCheckBoxMenuItem2.setSelected(true);
        jCheckBoxMenuItem2.setText("Filtre Flexion");
        jCheckBoxMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem2ActionPerformed(evt);
            }
        });
        jMenu_Affichage.add(jCheckBoxMenuItem2);

        jCheckBoxMenuItem3.setSelected(true);
        jCheckBoxMenuItem3.setText("Filtre Image");
        jCheckBoxMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem3ActionPerformed(evt);
            }
        });
        jMenu_Affichage.add(jCheckBoxMenuItem3);

        jMenuBar1.add(jMenu_Affichage);

        jMenu_Option.setMnemonic('o');
        jMenu_Option.setText("Options");

        jMenuItem_Preference.setText("Préférences");
        jMenuItem_Preference.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_PreferenceActionPerformed(evt);
            }
        });
        jMenu_Option.add(jMenuItem_Preference);

        jMenuBar1.add(jMenu_Option);

        jMenu_Aide.setMnemonic('a');
        jMenu_Aide.setText("Aide");

        jMenuItem_Aide.setText("Aide");
        jMenuItem_Aide.setEnabled(false);
        jMenu_Aide.add(jMenuItem_Aide);
        jMenu_Aide.add(jSeparator3);

        jMenuItem_Apropos.setText("À propos");
        jMenuItem_Apropos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_AproposActionPerformed(evt);
            }
        });
        jMenu_Aide.add(jMenuItem_Apropos);

        jMenuBar1.add(jMenu_Aide);

        setJMenuBar(jMenuBar1);

        getAccessibleContext().setAccessibleParent(this);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem_ImprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_ImprimerActionPerformed
        PrinterJob pj = PrinterJob.getPrinterJob();
        pj.setJobName("Imprimer planDessin");
        pj.setPrintable(new Printable() {

            @Override
            public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                if (pageIndex > 0) {
                    return Printable.NO_SUCH_PAGE;
                }

                Graphics2D g2;
                g2 = (Graphics2D) graphics;
                g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
                m_planDessin.paint(g2);
                return Printable.PAGE_EXISTS;
            }
        });
        if (pj.printDialog() == false) {
            return;
        }

        try {
            pj.print();
        } catch (PrinterException ex) {
        }
    }//GEN-LAST:event_jMenuItem_ImprimerActionPerformed

    private void jMenuItem_QuitterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_QuitterActionPerformed
        closeConfirmation();
    }//GEN-LAST:event_jMenuItem_QuitterActionPerformed

    private void jCheckBoxMenuItem_AfficherBarreOutilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem_AfficherBarreOutilActionPerformed
        if (jCheckBoxMenuItem_AfficherBarreOutil.isSelected()) {
            jtbBoiteOutil.setVisible(true);
        } else {
            jtbBoiteOutil.setVisible(false);
        }
    }//GEN-LAST:event_jCheckBoxMenuItem_AfficherBarreOutilActionPerformed

    private void jMenuItem_AproposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_AproposActionPerformed
        new JFrameAbout(this).setVisible(true);
    }//GEN-LAST:event_jMenuItem_AproposActionPerformed

    private void jtbBoiteOutilPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jtbBoiteOutilPropertyChange
        if (SwingConstants.VERTICAL == jtbBoiteOutil.getOrientation()) {
            jtbBoiteOutil.setPreferredSize(new Dimension(71, 332));
            jPanel1.setMaximumSize(new Dimension(49, 190));
            jPanel1.setPreferredSize(new Dimension(49, 190));
        } else {
            jtbBoiteOutil.setPreferredSize(new Dimension(332, 51));
            jPanel1.setMaximumSize(new Dimension(190, 49));
            jPanel1.setPreferredSize(new Dimension(190, 49));
        }
    }//GEN-LAST:event_jtbBoiteOutilPropertyChange

    private void jtbBoiteOutilMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtbBoiteOutilMouseDragged
        if (SwingConstants.VERTICAL == jtbBoiteOutil.getOrientation()) {
            jtbBoiteOutil.setOrientation(SwingConstants.VERTICAL);
        }
        if (SwingConstants.HORIZONTAL == jtbBoiteOutil.getOrientation()) {
            jtbBoiteOutil.setOrientation(SwingConstants.HORIZONTAL);
        }
    }//GEN-LAST:event_jtbBoiteOutilMouseDragged

    private void jMenuItem_PreferenceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_PreferenceActionPerformed
        new JFramePref(this, m_reseau).setVisible(true);
    }//GEN-LAST:event_jMenuItem_PreferenceActionPerformed

    private void jmiSupprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiSupprimerActionPerformed
        deleteEvent();
    }//GEN-LAST:event_jmiSupprimerActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        closeConfirmation();
    }//GEN-LAST:event_formWindowClosing

    private void jMenuItem_NouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_NouveauActionPerformed
        m_reseau.nouveauReseau();
        m_selectionElement.selectRien();
        setEnregistrement(false);
        m_planDessin.repaint();
    }//GEN-LAST:event_jMenuItem_NouveauActionPerformed

    private void jMenuItem_EffacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_EffacerActionPerformed
        deleteEvent();
    }//GEN-LAST:event_jMenuItem_EffacerActionPerformed

    private void m_planDessinMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_m_planDessinMouseReleased
        if (evt.isPopupTrigger()) {
            if (m_selectionElement.getObject() == null) {
                jmiPasseAvant.setVisible(false);
                jmiPasserArriere.setVisible(false);
                jmiSupprimer.setVisible(false);
                jSeparator10.setVisible(false);
                jSeparator13.setVisible(false);
            } else {
                jmiPasseAvant.setVisible(true);
                jmiPasserArriere.setVisible(true);
                jmiSupprimer.setVisible(true);
                jSeparator10.setVisible(true);
                jSeparator13.setVisible(true);
            }
            jpmPlanDessin.show(evt.getComponent(), evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_m_planDessinMouseReleased

    private void jmiInverserVueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiInverserVueActionPerformed
        threeWayJPanel1.invertPane();
    }//GEN-LAST:event_jmiInverserVueActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        float tailleFont = m_planDessin.getFontSize() - 1;
        m_planDessin.setFontSize(tailleFont);
        m_planDessin.repaint();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        float tailleFont = m_planDessin.getFontSize() + 1;
        m_planDessin.setFontSize(tailleFont);
        m_planDessin.repaint();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jCheckBoxMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem1ActionPerformed
        if (jCheckBoxMenuItem1.isSelected()) {
            m_selectionElement.setFiltreSelect(false, null, null, null);
        } else {
            m_selectionElement.setFiltreSelect(true, null, null, null);
        }
    }//GEN-LAST:event_jCheckBoxMenuItem1ActionPerformed

    private void jCheckBoxMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem2ActionPerformed
        if (jCheckBoxMenuItem2.isSelected()) {
            m_selectionElement.setFiltreSelect(null, null, false, null);
        } else {
            m_selectionElement.setFiltreSelect(null, null, true, null);
        }
    }//GEN-LAST:event_jCheckBoxMenuItem2ActionPerformed

    private void jCheckBoxMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem3ActionPerformed
        if (jCheckBoxMenuItem3.isSelected()) {
            m_selectionElement.setFiltreSelect(null, null, null, false);
        } else {
            m_selectionElement.setFiltreSelect(null, null, null, true);
        }
    }//GEN-LAST:event_jCheckBoxMenuItem3ActionPerformed

    private void jCheckBoxMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem4ActionPerformed
        if (jCheckBoxMenuItem4.isSelected()) {
            m_selectionElement.setFiltreSelect(null, false, null, null);
        } else {
            m_selectionElement.setFiltreSelect(null, true, null, null);
        }
    }//GEN-LAST:event_jCheckBoxMenuItem4ActionPerformed

    private void jmiAlignActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiAlignActionPerformed
        new JFrameAlign(this, m_selectionElement, m_reseau).setVisible(true);
    }//GEN-LAST:event_jmiAlignActionPerformed

    private void jMenuItem_SelectionnerToutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_SelectionnerToutActionPerformed
        List<Object> objects = new ArrayList<>();
        objects.addAll(m_reseau.getStations());
        objects.addAll(m_reseau.getArcs());
        objects.addAll(m_reseau.getGraphiques());
        for (Arc arc : m_reseau.getArcs()) {
            objects.addAll(arc.getFlex());
        }
        m_selectionElement.selectAll(objects, false);
    }//GEN-LAST:event_jMenuItem_SelectionnerToutActionPerformed

    private void jMenuItem_EffacerFondActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_EffacerFondActionPerformed
        m_planDessin.effacerArrierePlan();
    }//GEN-LAST:event_jMenuItem_EffacerFondActionPerformed

    private void jchk_ImageFondActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jchk_ImageFondActionPerformed
        m_planDessin.setImageFondVisible(jchk_ImageFond.isSelected());
    }//GEN-LAST:event_jchk_ImageFondActionPerformed

    public void setEnregistrement(boolean actif) {
        jMenuItem_Enregistrer.setEnabled(actif);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnInsererImage;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem2;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem3;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem4;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem_Activer;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem_Afficher;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem_AfficherBarreOutil;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JMenuItem jMenuItem_Aide;
    private javax.swing.JMenuItem jMenuItem_Annuler;
    private javax.swing.JMenuItem jMenuItem_Apropos;
    private javax.swing.JMenuItem jMenuItem_Coller;
    private javax.swing.JMenuItem jMenuItem_Copier;
    private javax.swing.JMenuItem jMenuItem_Couper;
    private javax.swing.JMenuItem jMenuItem_Effacer;
    private javax.swing.JMenuItem jMenuItem_EffacerFond;
    private javax.swing.JMenuItem jMenuItem_Enregistrer;
    private javax.swing.JMenuItem jMenuItem_EnregistrerSous;
    private javax.swing.JMenuItem jMenuItem_Exporter;
    private javax.swing.JMenuItem jMenuItem_Importer;
    private javax.swing.JMenuItem jMenuItem_Imprimer;
    private javax.swing.JMenuItem jMenuItem_Nouveau;
    private javax.swing.JMenuItem jMenuItem_Ouvrir;
    private javax.swing.JMenuItem jMenuItem_PasserArriere;
    private javax.swing.JMenuItem jMenuItem_PasserAvant;
    private javax.swing.JMenuItem jMenuItem_Preference;
    private javax.swing.JMenuItem jMenuItem_Quitter;
    private javax.swing.JMenuItem jMenuItem_Retablir;
    private javax.swing.JMenuItem jMenuItem_SelectionnerTout;
    private javax.swing.JMenuItem jMenuItem_Zoom;
    private javax.swing.JMenu jMenu_Affichage;
    private javax.swing.JMenu jMenu_Aide;
    private javax.swing.JMenu jMenu_Edition;
    private javax.swing.JMenu jMenu_Fichier;
    private javax.swing.JMenu jMenu_Option;
    private javax.swing.JPanel jPanel1;
    private PerformNet.gui.panel.PanneauProprietesAffichage jPanelAffichage1;
    private javax.swing.JPanel jPanel_empty;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator10;
    private javax.swing.JPopupMenu.Separator jSeparator11;
    private javax.swing.JPopupMenu.Separator jSeparator12;
    private javax.swing.JPopupMenu.Separator jSeparator13;
    private javax.swing.JPopupMenu.Separator jSeparator14;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JPopupMenu.Separator jSeparator7;
    private javax.swing.JPopupMenu.Separator jSeparator8;
    private javax.swing.JPopupMenu.Separator jSeparator9;
    private javax.swing.JCheckBoxMenuItem jchk_ImageFond;
    private javax.swing.JMenu jmTaillePolice;
    private javax.swing.JMenuItem jmiAlign;
    private javax.swing.JMenuItem jmiInsererArc;
    private javax.swing.JMenuItem jmiInsererImage;
    private javax.swing.JMenuItem jmiInsererStation;
    private javax.swing.JMenuItem jmiInverserVue;
    private javax.swing.JMenuItem jmiPasseAvant;
    private javax.swing.JMenuItem jmiPasserArriere;
    private javax.swing.JMenuItem jmiSelection;
    private javax.swing.JMenuItem jmiSupprimer;
    private PerformNet.gui.control.JSplitButton jpBtnRedo;
    private PerformNet.gui.control.JSplitButton jpBtnUndo;
    private javax.swing.JPopupMenu jpmPlanDessin;
    private javax.swing.JToggleButton jtBnInsererArc;
    private javax.swing.JToggleButton jtBnSelection;
    private javax.swing.JToggleButton jtBtnInsererStation;
    private javax.swing.JToolBar jtbBoiteOutil;
    private PerformNet.gui.plan.PlanDessin m_planDessin;
    private PerformNet.gui.container.ThreeWayJPanel threeWayJPanel1;
    // End of variables declaration//GEN-END:variables
}
