/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet.gui;

import java.awt.Dimension;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Sert à convertir des formes géométriques exprimées en mètres à une échelle
 * spécique en pixel, et vice versa.
 *
 * @author expresso
 */
public class Zoom {

    private final double m_echelleDefaut = 25; //En pixels par mètres
    private double m_zoom = m_echelleDefaut;

    public void setZoomPourcent(int zoom_pourcent) {
        m_zoom = m_echelleDefaut * zoom_pourcent / 100;
    }
    
    public int getZoomPourcent() {
        return (int)(m_zoom / m_echelleDefaut * 100);
    }
    
    public double getZoomFact() {
        return 1/m_zoom;
    }

    public Point2D versMetres(Point2D pt) {
        return new Point2D.Double(
                pt.getX() / m_zoom,
                pt.getY() / m_zoom);
    }

    public Rectangle2D versMetres(Rectangle2D rect) {
        return new Rectangle2D.Double(
                rect.getX() / m_zoom,
                rect.getY() / m_zoom,
                rect.getWidth() / m_zoom,
                rect.getHeight() / m_zoom
        );
    }
    
    public Line2D versPixels(Line2D line) {
        return new Line2D.Double(
                line.getX1() * m_zoom,
                line.getY1() * m_zoom,
                line.getX2() * m_zoom,
                line.getY2() * m_zoom);
    }
    
    public List<Line2D> versPixels(List<Line2D> lines) {
        List<Line2D> new_lines = new ArrayList();
        for(Line2D line : lines) {
            new_lines.add(versPixels(line));
        }
        return new_lines;
    }

    public Rectangle2D versPixels(Rectangle2D rect) {
        return new Rectangle2D.Double(
                rect.getX() * m_zoom,
                rect.getY() * m_zoom,
                rect.getWidth() * m_zoom,
                rect.getHeight() * m_zoom);
    }

    public Dimension versPixels(Dimension d) {
        return new Dimension(
                (int) (d.getWidth() * m_zoom),
                (int) (d.getHeight() * m_zoom));
    }

   public Point2D versPixels(Point2D d) {
        return new Point2D.Double(
                (int) (d.getX() * m_zoom),
                (int) (d.getY() * m_zoom));
    }
   
   public double versPixels(double x) {
       return x * m_zoom;
   }

    
}
