/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet.gui.action;

import PerformNet.gui.MainJFrame;
import static PerformNet.gui.action.FilterSetter.initImageFilter;
import PerformNet.gui.plan.PlanDessin;
import java.awt.event.ActionEvent;
import java.io.IOException;
import static java.nio.file.Files.isReadable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

/**
 *
 * @author Expresso
 */
public class InsererImage extends AbstractAction {
    private static final long serialVersionUID = -4148228756142295734L;

    private PlanDessin m_plan;
    private MainJFrame m_jframe;
    private String m_fileName = null;

    private final ImageIcon iconSmall = new ImageIcon(getClass().getResource("/img/icones_menu/insererImage_small.png"));
    private final ImageIcon iconLarge = new ImageIcon(getClass().getResource("/img/icones_menu/insererImage.png"));

    public InsererImage(MainJFrame m_jframe, PlanDessin plan) {

        putValue(NAME, "Inserer Image");
        putValue(SMALL_ICON, iconSmall);
        putValue(LARGE_ICON_KEY, iconLarge);

        m_plan = plan;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        m_fileName = "";

        JFileChooser chooser = new JFileChooser();

        initImageFilter(chooser, true, true, true, true, true);

        int returnVal = chooser.showOpenDialog(m_jframe);

        if (returnVal == JFileChooser.APPROVE_OPTION) {

            try {
                m_fileName = chooser.getSelectedFile().getCanonicalPath();

            } catch (IOException ex) {
                Logger.getLogger(MainJFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            return;
        }

        Path fichierValide;

        if (m_fileName != null) {
            fichierValide = Paths.get(m_fileName);

            if (isReadable(fichierValide)) {

                if (m_fileName.toUpperCase().endsWith(".JPG")
                        || m_fileName.toUpperCase().endsWith(".PNG")
                        || m_fileName.toUpperCase().endsWith(".GIF")
                        || m_fileName.toUpperCase().endsWith(".BMP")
                        || m_fileName.toUpperCase().endsWith(".SVG")) {
                    m_plan.importerGraphique(m_fileName);
                }
                m_fileName = "";
            }
        }
    }
}
