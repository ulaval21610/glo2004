/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet.gui.action;

import PerformNet.gui.MainJFrame;
import PerformNet.gui.selection.SelectionElement;
import PerformNet.modele.Reseau;
import java.awt.event.ActionEvent;
import java.io.IOException;
import static java.nio.file.Files.isReadable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * classe qui permet d'ouvrir un fichier d'un projet pré-sauvegardé
 *
 * @author Expresso
 */
public class OuvertureFichier extends AbstractAction {
    private static final long serialVersionUID = 8376309422514147296L;

    private transient Reseau m_reseau;
    private String m_fileName = null;
    private SelectionElement m_select;

    private MainJFrame m_jframe;

    /**
     * constructeur permet d'extraire le mainjframe et le planDessin mis en
     * paramètre
     *
     * @param jframe
     * @param reseau
     * @param select
     */
    public OuvertureFichier(MainJFrame jframe, Reseau reseau, SelectionElement select) {
        m_reseau = reseau;
        m_jframe = jframe;
        m_select = select;

        putValue(NAME, "Ouvrir");
        putValue(ACCELERATOR_KEY, javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
    }
    /* cette fonction permet d'associer l'action d'ouvrir un projet dans le menu ouvrir
     *au loading comme tel du projet faite par la fonction loadFile du planDessin
     */

    @Override
    public void actionPerformed(ActionEvent e) {

        JFileChooser chooser = new JFileChooser();

        FileNameExtensionFilter pnFilter = new FileNameExtensionFilter("PerformNet (*.pn)", ".pn");

        chooser.setDialogTitle("Ouvrir");
        //chooser.setAcceptAllFileFilterUsed(false);
        //chooser.addChoosableFileFilter(pnFilter);
        //chooser.setFileFilter(pnFilter);
        int returnVal = chooser.showOpenDialog(m_jframe);

        if (returnVal == JFileChooser.APPROVE_OPTION) {

            try {
                m_fileName = chooser.getSelectedFile().getCanonicalPath();
                // System.out.println(chooser.getSelectedFile().getCanonicalPath());

            } catch (IOException ex) {
                Logger.getLogger(MainJFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Path fichierValide;

        if (m_fileName != null) {
            fichierValide = Paths.get(m_fileName);

            if (isReadable(fichierValide)) {
                
                m_reseau.loadFile(m_fileName);
                m_jframe.setEnregistrement(true);
                m_select.selectRien();
                m_fileName = "";
            }
        }
    }
}
