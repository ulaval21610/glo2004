/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet.gui.action;

import PerformNet.gui.plan.PlanDessin;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import static javax.swing.Action.SELECTED_KEY;
import javax.swing.ImageIcon;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *classe qui sert à passer en mode insérer arc
 * @author Expresso
 */
public class InsererArc extends AbstractAction {
    private static final long serialVersionUID = -7226861172733120232L;

    private PlanDessin m_plan;
    private AbstractButton m_stateButton;
    private static Boolean m_etat = false;

    private final ImageIcon iconSmall = new ImageIcon(getClass().getResource("/img/icones_menu/insererArc_small.png"));
    private final ImageIcon iconLarge = new ImageIcon(getClass().getResource("/img/icones_menu/insererArc.png"));

    /**
     *constructeur qui sert à extraire le planDessin et le AbstractButton repreésentant le bouton pour l'action insérer un arc
     * @param plan
     * @param stateButton
     */
    public InsererArc(PlanDessin plan, AbstractButton stateButton) {
        m_plan = plan;
        m_stateButton = stateButton;
        putValue(NAME, "Inserer Arc");
        putValue(SMALL_ICON, iconSmall);
        putValue(LARGE_ICON_KEY, iconLarge);

        /*observateur de changement d'état du bouton stateButton
        *Si l'état du bouton est activé, passe alors en mode non activé lors d'un ChangeEvent représentant un clic de souris sur le 
        *bouton stateButton.
        *
        */
        m_stateButton.addChangeListener(new ChangeListener() {
            
            @Override
            public void stateChanged(ChangeEvent e) {
                if (!m_stateButton.isSelected() && m_etat == true) {
                    m_etat = false;
                }
            }
        });
    }

    /**
     *active l'état d'insertion d'arc dans le planDessin
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        m_etat = m_etat != true;
        putValue(SELECTED_KEY, m_etat);

        m_plan.changeEtat(
                PlanDessin.Etat.INSERTION_ARC,
                m_etat);
    }
}
