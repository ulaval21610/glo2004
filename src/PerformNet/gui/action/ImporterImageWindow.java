/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet.gui.action;

import PerformNet.gui.MainJFrame;
import static PerformNet.gui.action.FilterSetter.initImageFilter;
import PerformNet.gui.plan.PlanDessin;
import java.awt.event.ActionEvent;
import java.io.IOException;
import static java.nio.file.Files.isReadable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;

/**
 * classe qui sert à instancier l'action d'importer une image de fond pour le
 * plan du réesau
 *
 * @author Expresso
 */
public class ImporterImageWindow extends AbstractAction {
    private static final long serialVersionUID = -2624759556468296425L;

    private String m_fileName = null;
    private PlanDessin m_plan;
    private MainJFrame m_jframe;

    /**
     *
     * Constructeur qui sert à extraire le mainjframe et le plan dessin quand on
     * l'appelle
     *
     * @param jframe
     * @param plan
     */
    public ImporterImageWindow(MainJFrame jframe, PlanDessin plan) {
        m_plan = plan;
        m_jframe = jframe;
        putValue(NAME, "Importer Image de Fond");
    }


    /* activateur d'importation d'image de fond. Quand on démarre l'action, le planDessin passé 
     * en paramètre du constructeur se voit associé une image de fond à partir d'un path où est situé
     * l'image 
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        JFileChooser chooser = new JFileChooser();

        initImageFilter(chooser, true, true, true, true, true);

        chooser.setDialogTitle("Insérer une image de fond");

        int returnVal = chooser.showOpenDialog(m_jframe);

        if (returnVal == JFileChooser.APPROVE_OPTION) {

            try {
                m_fileName = chooser.getSelectedFile().getCanonicalPath();

            } catch (IOException ex) {
                Logger.getLogger(MainJFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        Path fichierValide;

        if (m_fileName != null) {
            fichierValide = Paths.get(m_fileName);

            if (isReadable(fichierValide)) {

                if (m_fileName.toUpperCase().endsWith(".JPG")
                        || m_fileName.toUpperCase().endsWith(".PNG")
                        || m_fileName.toUpperCase().endsWith(".GIF")
                        || m_fileName.toUpperCase().endsWith(".BMP")) {
                    m_plan.importerImage(m_fileName);
                }
                if (m_fileName.toUpperCase().endsWith(".SVG")) {
                    m_plan.importerImageSVG(m_fileName);
                }
                m_fileName = "";
            }
        }
    }
}
