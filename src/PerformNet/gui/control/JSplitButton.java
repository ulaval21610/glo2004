/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet.gui.control;

import PerformNet.gui.panel.Direction.direction;
import static PerformNet.gui.panel.Direction.direction.EAST;
import static PerformNet.gui.panel.Direction.direction.SOUTH;
import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.Action;
import javax.swing.JMenuItem;

/**
 *
 * @author Expresso
 */
public class JSplitButton extends javax.swing.JPanel {
    private static final long serialVersionUID = -7098080271228774760L;

    private String m_downArrow = "▼";
    private String m_rightArrow = "►";
    //˅˄˃˂

    private ArrayList<String> m_historique = new ArrayList<>();

    /**
     * Creates new SplitButton
     */
    public JSplitButton() {
        initComponents();
    }

    public JSplitButton(direction arrowDirection) {
        initComponents();
        
        switch (arrowDirection) {
            case EAST:
                jlbArrow.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/icones_menu/downArrow.png")));
                this.add(jlbArrow, BorderLayout.EAST);
                break;
            case SOUTH:
                // add icon
                //jlbArrow.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/icones_menu/downArrow.png"))); 
                this.add(jlbArrow, BorderLayout.SOUTH);
                break;
        }
    }

    public void setButtonAction(Action buttonAction) {
        jButton1.setAction(buttonAction);
        jButton1.setText("");
    }

    public void jmenuItemFactory(ArrayList<String> historique) {
        for (String itemHistorique : historique) {
            jPopupMenu1.add(new JMenuItem(itemHistorique));
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jlbArrow = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();

        jMenuItem1.setText("jMenuItem1");
        jPopupMenu1.add(jMenuItem1);

        jMenuItem2.setText("jMenuItem2");
        jPopupMenu1.add(jMenuItem2);

        jlbArrow.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlbArrow.setEnabled(false);
        jlbArrow.setMaximumSize(new java.awt.Dimension(14, 0));
        jlbArrow.setMinimumSize(new java.awt.Dimension(14, 0));
        jlbArrow.setPreferredSize(new java.awt.Dimension(14, 0));
        jlbArrow.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbArrowMouseClicked(evt);
            }
        });

        jPanel1.setMinimumSize(new java.awt.Dimension(20, 0));
        jPanel1.setPreferredSize(new java.awt.Dimension(20, 0));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 719, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setLayout(new java.awt.BorderLayout());

        jButton1.setDefaultCapable(false);
        jButton1.setFocusPainted(false);
        jButton1.setMaximumSize(new java.awt.Dimension(45, 41));
        jButton1.setMinimumSize(new java.awt.Dimension(45, 41));
        jButton1.setPreferredSize(new java.awt.Dimension(45, 41));
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jButton1MouseReleased(evt);
            }
        });
        add(jButton1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jlbArrowMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbArrowMouseClicked
        //jPopupMenu1.show(evt.getComponent(), jButton1.getX() - jlbArrow.getX(), jButton1.getY() + jButton1.getHeight());
    }//GEN-LAST:event_jlbArrowMouseClicked

    private void jButton1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseReleased
       // if (evt.isPopupTrigger()) {
        //    jPopupMenu1.show(evt.getComponent(), jButton1.getX(), jButton1.getY() + jButton1.getHeight());
       // }
    }//GEN-LAST:event_jButton1MouseReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButton1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JLabel jlbArrow;
    // End of variables declaration//GEN-END:variables
}
