/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet.gui;

import PerformNet.gui.container.ThreeWayJPanel;
import PerformNet.gui.panel.PanneauProprietesAffichage;
import PerformNet.modele.Reseau;
import java.util.prefs.Preferences;

/**
 *
 * @author Expresso
 */
class UserPrefs {

    private static Preferences prefs;

    private static final String m_autoBack = "AutoBack";
    private static final String m_boiteOutil = "BoiteOutil";
    private static final String m_dessinGauche = "dessinGauche";
    private static final String m_grille = "Grille";
    private static final String m_magnetique = "Magnetique";
    private static final String m_grilleDim = "Zoom";
    private static final String m_zoomDim = "ZoomDim";

    private final int m_zoomDimDefault = 100;
    private final int m_grilleDimDefault = 1;
    private final int m_autoBackDefault = 5;
    private final boolean m_grilleDefault = true;
    private final boolean m_magnetiqueDefault = true;
    private final boolean m_boiteOutilDefault = true;
    private final boolean m_dessinGaucheDefault = true;

    private UserPrefs() {
        prefs = Preferences.userRoot().node(this.getClass().getName());
    }

    public static UserPrefs getInstance() {
        return UserPrefsSingleton.INSTANCE;
    }

    private static class UserPrefsSingleton {
        private static final UserPrefs INSTANCE = new UserPrefs();
    }

    void savePreference(Integer zoomDim, Integer grilleDim, Boolean grille,
            Boolean magnetique, Boolean boiteOutil,
            Boolean dessinGauche, Integer autoBack) {

        prefs.getBoolean(m_grille, m_grilleDefault);
        prefs.getBoolean(m_magnetique, m_magnetiqueDefault);
        prefs.getBoolean(m_boiteOutil, m_boiteOutilDefault);
        prefs.getBoolean(m_dessinGauche, m_dessinGaucheDefault);
        prefs.getInt(m_grilleDim, m_grilleDimDefault);
        prefs.getInt(m_zoomDim, m_zoomDimDefault);
        prefs.getInt(m_autoBack, m_autoBackDefault);

        if (grilleDim != null) {
            prefs.putInt(m_grilleDim, grilleDim);
        }
        if (grille != null) {
            prefs.putBoolean(m_grille, grille);
        }
        if (boiteOutil != null) {
            prefs.putBoolean(m_boiteOutil, boiteOutil);
        }
        if (magnetique != null) {
            prefs.putBoolean(m_magnetique, magnetique);
        }
        if (zoomDim != null) {
            prefs.putInt(m_zoomDim, zoomDim);
        }
        if (autoBack != null) {
            prefs.putInt(m_autoBack, autoBack);
        }
        if (dessinGauche != null) {
            prefs.putBoolean(m_dessinGauche, dessinGauche);
        }
    }

    void loadPreference(MainJFrame mJFrame, Reseau rs, PanneauProprietesAffichage ppa,
            ThreeWayJPanel twp) {

        rs.setGrille(prefs.getInt(m_grilleDim, m_grilleDimDefault),
                prefs.getBoolean(m_grille, m_grilleDefault),
                prefs.getBoolean(m_magnetique, m_magnetiqueDefault));

        rs.setAutoBackUp(prefs.getInt(m_autoBack, m_autoBackDefault));
        ppa.setZoom(prefs.getInt(m_zoomDim, m_zoomDimDefault));
        mJFrame.setBoiteOutil(prefs.getBoolean(m_boiteOutil, m_boiteOutilDefault));
        if(!prefs.getBoolean(m_dessinGauche, m_dessinGaucheDefault)){
            twp.invertPane();
        }
    }

    void getPreference(JFramePref jfp) {
        jfp.initValues(prefs.getInt(m_zoomDim, m_zoomDimDefault),
                prefs.getInt(m_grilleDim, m_grilleDimDefault),
                prefs.getBoolean(m_grille, m_grilleDefault),
                prefs.getBoolean(m_magnetique, m_magnetiqueDefault),
                prefs.getBoolean(m_boiteOutil, m_boiteOutilDefault),
                prefs.getBoolean(m_dessinGauche, m_dessinGaucheDefault),
                prefs.getInt(m_autoBack, m_autoBackDefault));
    }
}
