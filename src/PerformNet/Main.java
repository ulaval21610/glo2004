/*
* Copyright (c) 2014, Philippe Olivier, François Le Monnier-Lalonde, 
*                     François Moreau, Dominique Tremblay
*
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
*    this list of conditions and the following disclaimer in the documentation 
*    and/or other materials provided with the distribution.
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
*    may be used to endorse or promote products derived from this software 
*    without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
* POSSIBILITY OF SUCH DAMAGE.
*/

package PerformNet;

import PerformNet.gui.MainJFrame;
import PerformNet.modele.Reseau;
import java.awt.EventQueue;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * Contrôleur de l'application.
 *
 * Est responsable de l'instanciation de la vue, du modèle et de la délégation
 * des requêtes de la vue vers le modèle.
 *
 * @author Expresso
 */
public class Main {

    public static void main(String[] args) {
        MainJFrame.setDefaultLookAndFeelDecorated(true);
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel("org.pushingpixels.substance.api.skin.SubstanceGraphiteLookAndFeel");
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
                    System.out.println("Substance Graphite failed to initialize");
                }
            }
        });

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

                System.setProperty("sun.java2d.opengl", "true");

                Reseau reseau = new Reseau();

//                //TEMP pour tester l'affichage
//                int id1 = reseau.addStation(new Point2D.Double(4, 4));
//                int id2 = reseau.addStation(new Point2D.Double(15, 4));
//                int id3 = reseau.addStation(new Point2D.Double(30, 4));
//
//                reseau.addArc(
//                        reseau.findStation(id1),
//                        reseau.findStation(id2));
//
//                reseau.addArc(
//                        reseau.findStation(id2),
//                        reseau.findStation(id3));
//
//                reseau.addArc(
//                        reseau.findStation(ID_ARRIVEE),
//                        reseau.findStation(id1));
//
//                reseau.addArc(
//                        reseau.findStation(id3),
//                        reseau.findStation(ID_SORTIE));
                MainJFrame window = new MainJFrame(reseau);

                window.setVisible(true);

            }
        });
    }

}
